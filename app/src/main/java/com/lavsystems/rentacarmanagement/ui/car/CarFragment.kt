package com.lavsystems.rentacarmanagement.ui.car

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.lavsystems.rentacarmanagement.R
import com.lavsystems.rentacarmanagement.data.api.service.CarService
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptorImpl
import com.lavsystems.rentacarmanagement.data.datasource.CarDataSourceImpl
import com.lavsystems.rentacarmanagement.data.repository.CarRepository
import com.lavsystems.rentacarmanagement.data.repository.CarRepositoryImpl
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferencesImpl
import com.lavsystems.rentacarmanagement.databinding.FragmentCarBinding
import com.lavsystems.rentacarmanagement.model.Car
import com.lavsystems.rentacarmanagement.ui.car.CarDialogFragment.Companion.CAR_DIALOG_FRAGMENT_FLAG
import com.lavsystems.rentacarmanagement.ui.home.CommunicationViewModel
import com.lavsystems.rentacarmanagement.ui.login.LoginActivity
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showDialog
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import com.lavsystems.rentacarmanagement.ui.utils.RecyclerListener
import com.lavsystems.rentacarmanagement.ui.utils.TypeUserEnum


class CarFragment : Fragment(), RecyclerListener {
    private lateinit var binding: FragmentCarBinding
    private lateinit var carAdapter: CarAdapter
    private lateinit var repository: CarRepository
    private lateinit var viewModel: CarViewModel
    private val communicationViewModel: CommunicationViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        repository = CarRepositoryImpl(
            CarDataSourceImpl(
                SharedPreferencesImpl(requireContext()), CarService(
                    ConnectivityInterceptorImpl(requireContext()),
                    com.lavsystems.rentacarmanagement.BuildConfig.SERVER_URL
                )
            )
        )
        setUpViewModel()
        setUpObserverViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCarBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initAdView()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCars()
        binding.avCar.resume()
    }

    override fun onPause() {
        binding.avCar.pause()
        super.onPause()
    }

    private fun initAdView() {
        binding.avCar.loadAd(AdRequest.Builder().build())
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            CarViewModelFactory(repository)
        )[CarViewModel::class.java]
    }

    private fun setUpObserverViewModel() {
        with(viewModel) {
            cars.observe(this@CarFragment) { state ->
                handleUiCars(state)
            }
            sessionExpired.observe(requireActivity()) { event ->
                event.getContentIfNotHandled()?.let {
                    ComponentUtils.showToast(requireContext(), getString(R.string.expired_session))
                    goToLogin()
                }
            }
        }
        with(communicationViewModel) {
            searchText.observe(this@CarFragment) { text ->
                carAdapter.filterByBrand(text)
            }

            isCreateCar.observe(this@CarFragment) { isCreate ->
                isCreate.getContentIfNotHandled()?.let {
                    if (it) {
                        openCarFragmentDialog()
                    }
                }
            }
        }
    }

    private fun initRecyclerView() {
        carAdapter = CarAdapter(this, communicationViewModel.isOnlyRead())
        val linearLayoutManager = getLinearLayoutManager()
        with(binding.rvCar) {
            layoutManager = linearLayoutManager
            adapter = carAdapter
            addItemDecoration(getDividerItemDecoration(linearLayoutManager))
        }
    }

    private fun getLinearLayoutManager() = LinearLayoutManager(requireContext())

    private fun getDividerItemDecoration(linearLayoutManager: LinearLayoutManager) =
        DividerItemDecoration(
            requireContext(),
            linearLayoutManager.orientation
        )

    private fun handleUiCars(uiState: DataState<MutableList<Car>>) {
        when (uiState) {
            is DataState.Success<MutableList<Car>> -> {
                if (uiState.data.isEmpty()) {
                    handlerEmptyVisibility(true)
                    handlerRecyclerVisibility(false)
                } else {
                    carAdapter.setCars(uiState.data)
                    handlerEmptyVisibility(false)
                    handlerRecyclerVisibility(true)
                }
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(false)
            }
            is DataState.Error -> {
                handlerErrorVisibility(true, uiState.throwable.message ?: EMPTY_STRING)
                handlerEmptyVisibility(false)
                handlerProgressBarVisibility(false)
                handlerRecyclerVisibility(false)
            }
            is DataState.Loading -> {
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerEmptyVisibility(false)
                handlerProgressBarVisibility(true)
                handlerRecyclerVisibility(false)
            }
            is DataState.Idle -> Unit
        }
    }

    private fun handlerProgressBarVisibility(show: Boolean) {
         binding.iProgressBar.progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun handlerRecyclerVisibility(show: Boolean) {
         binding.rvCar.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun handlerErrorVisibility(show: Boolean, error: String) {
        with(binding) {
            iGenericError.clGenericError.visibility = if (show) View.VISIBLE else View.GONE
            iGenericError.tvMessage.text = error
            iGenericError.btnClose.setOnClickListener {
                iGenericError.clGenericError.visibility = View.GONE
                viewModel.getCars()
            }
        }
    }

    private fun handlerEmptyVisibility(show: Boolean) {
        binding.tvCarEmpty.visibility = if(show) View.VISIBLE else View.GONE
    }

    override fun onClick(id: String) {
        openCarFragmentDialog(id)
    }

    override fun onMenuClickEdit(id: String) {
        openCarFragmentDialog(id)
    }

    override fun onMenuClickDelete(id: String) {
        showDialog(
            requireContext(), getString(R.string.delete_car_message_dialog), getString(
                R.string.accept_title
            ), getString(R.string.cancel_title)
        ) {
            deleteCar(id)
        }
    }

    private fun deleteCar(id: String) {
        viewModel.deleteCar(id)
    }

    private fun openCarFragmentDialog(id: String = "") {
        CarDialogFragment.newInstance(
            id,
            communicationViewModel.user?.type?.code == TypeUserEnum.READ.code
        ).show(parentFragmentManager, CAR_DIALOG_FRAGMENT_FLAG)
    }

    private fun goToLogin(){
        startActivity(Intent(requireContext(), LoginActivity::class.java))
        requireActivity().finish()
    }

    override fun onDestroy() {
        binding.avCar.destroy()
        super.onDestroy()
    }

    companion object {
        const val EMPTY_STRING = ""
    }
}
