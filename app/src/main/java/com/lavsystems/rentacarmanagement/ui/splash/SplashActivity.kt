package com.lavsystems.rentacarmanagement.ui.splash

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.lavsystems.rentacarmanagement.BuildConfig
import com.lavsystems.rentacarmanagement.data.api.service.LoginService
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptorImpl
import com.lavsystems.rentacarmanagement.data.datasource.SplashDataSourceImpl
import com.lavsystems.rentacarmanagement.data.repository.SplashRepository
import com.lavsystems.rentacarmanagement.data.repository.SplashRepositoryImpl
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferencesImpl
import com.lavsystems.rentacarmanagement.databinding.ActivitySplashBinding
import com.lavsystems.rentacarmanagement.model.User
import com.lavsystems.rentacarmanagement.ui.home.HomeActivity
import com.lavsystems.rentacarmanagement.ui.login.LoginActivity
import com.lavsystems.rentacarmanagement.ui.utils.DataState

class SplashActivity : AppCompatActivity() {

    private lateinit var viewModel: SplashViewModel
    private lateinit var binding: ActivitySplashBinding
    private lateinit var repository: SplashRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)
        repository = SplashRepositoryImpl(
            SplashDataSourceImpl(
                SharedPreferencesImpl(this), LoginService(ConnectivityInterceptorImpl(this), BuildConfig.AUTH_SERVER_URL)
            )
        )
        setUpViewModel()
        setUpObserver()
    }

    private fun setUpViewModel() {
        viewModel =
            ViewModelProvider(
                this,
                SplashViewModelFactory(repository)
            )[SplashViewModel::class.java]
    }

    private fun setUpObserver() {
        with(viewModel) {
            doAutoLogin()
            user.observe(this@SplashActivity) { state ->
                handleUiUser(state)
            }
        }
    }

    private fun handleUiUser(uiState: DataState<User>) {
        when (uiState) {
            is DataState.Success<User> -> {
                saveUser(uiState.data)
                startActivity(Intent(this, HomeActivity::class.java))
            }
            is DataState.Error -> {
                startActivity(Intent(this, LoginActivity::class.java))
            }
            is DataState.Loading -> {
            }
            is DataState.Idle -> Unit
        }
    }

    private fun saveUser(user: User) {
        viewModel.saveUser(user)
    }
}