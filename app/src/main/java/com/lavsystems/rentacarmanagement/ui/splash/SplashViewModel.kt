package com.lavsystems.rentacarmanagement.ui.splash

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lavsystems.rentacarmanagement.data.repository.SplashRepository
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.User
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class SplashViewModel(private val repository: SplashRepository) : ViewModel() {
    private val _user: MutableLiveData<DataState<User>> =
        MutableLiveData(DataState.Idle)
    val user: LiveData<DataState<User>> = _user

    fun doAutoLogin(){
        viewModelScope.launch {
            repository.doAutoLogin().onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _user.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _user.value = DataState.Success(it.data.data ?: User())
                    }
                    is Response.Error -> {
                        _user.value = DataState.Loading(loading = false)
                        _user.value = DataState.Error(it.exception)
                    }
                }
            }.launchIn(this)
        }
    }

    fun saveUser(user: User) {
        viewModelScope.launch {
            repository.saveUser(user).launchIn(this)
        }
    }
}