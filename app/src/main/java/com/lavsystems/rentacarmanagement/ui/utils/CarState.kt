package com.lavsystems.rentacarmanagement.ui.utils

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
interface CarState {
    fun getState(): String
    fun getColor(): Int

    companion object {
        const val AVAILABLE = "DISPONIBLE"
        const val RESERVED = "RESERVADO"
        const val RESERVED_TODAY = "RESERVADO HOY"
        const val ERROR = "ERROR DESCONOCIDO"
    }
}