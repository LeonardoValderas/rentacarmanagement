package com.lavsystems.rentacarmanagement.ui.utils

interface RecyclerListener {
    fun onClick(id: String)
    fun onMenuClickEdit(id: String)
    fun onMenuClickDelete(id: String)
}