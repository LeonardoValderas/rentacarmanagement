package com.lavsystems.rentacarmanagement.ui.booking

import android.view.View
import com.lavsystems.rentacarmanagement.ui.utils.ExpandableRecyclerViewAdapter
import com.lavsystems.rentacarmanagement.ui.utils.RecyclerListener

class BookingDetailsViewHolder(view: View, private val listener: RecyclerListener) :
    ExpandableRecyclerViewAdapter.ExpandableViewHolder(view)