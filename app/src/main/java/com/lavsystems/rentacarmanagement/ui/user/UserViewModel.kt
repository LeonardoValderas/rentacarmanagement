package com.lavsystems.rentacarmanagement.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lavsystems.rentacarmanagement.data.api.utils.TokenException
import com.lavsystems.rentacarmanagement.data.dto.UserDTO
import com.lavsystems.rentacarmanagement.data.repository.UserRepository
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.User
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import com.lavsystems.rentacarmanagement.ui.utils.EventWrapper
import com.lavsystems.rentacarmanagement.ui.utils.SuccessAction
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class UserViewModel(private val repository: UserRepository) : ViewModel() {

    private val _users: MutableLiveData<DataState<MutableList<User>>> =
        MutableLiveData(DataState.Idle)
    val users: LiveData<DataState<MutableList<User>>> = _users

    private val _userDTO: MutableLiveData<DataState<UserDTO>> =
        MutableLiveData(DataState.Idle)
    val userDTO: LiveData<DataState<UserDTO>> = _userDTO

    private val _user: MutableLiveData<DataState<User>> =
        MutableLiveData(DataState.Idle)
    val user: LiveData<DataState<User>> = _user

    private val _sessionExpired = MutableLiveData<EventWrapper<Unit>>()
    val sessionExpired: LiveData<EventWrapper<Unit>> = _sessionExpired

    private val _saveSuccess = MutableLiveData<EventWrapper<SuccessAction>>()
    val saveSuccess: LiveData<EventWrapper<SuccessAction>> = _saveSuccess

    private val _updateSuccess = MutableLiveData<EventWrapper<SuccessAction>>()
    val updateSuccess: LiveData<EventWrapper<SuccessAction>> = _updateSuccess

    fun getUsers() {
        viewModelScope.launch {
            repository.getUsers().onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _users.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _users.value = DataState.Success(it.data.data ?: mutableListOf())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _users.value = DataState.Loading(loading = false)
                            _users.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun getFormUser(id: String) {
        viewModelScope.launch {
            repository.getFormUser(id).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _userDTO.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _userDTO.value = DataState.Success(it.data.data ?: UserDTO())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _userDTO.value = DataState.Loading(loading = false)
                            _userDTO.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun saveUser(user: User) {
        viewModelScope.launch {
            repository.saveUser(user).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _users.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _users.value = DataState.Success(it.data.data ?: mutableListOf())
                        _saveSuccess.value = EventWrapper(SuccessAction(true, ""))
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _users.value = DataState.Loading(loading = false)
                            _users.value = DataState.Error(it.exception)
                            _saveSuccess.value =
                                EventWrapper(SuccessAction(false, it.exception.message ?: ""))
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun updateUser(user: User) {
        viewModelScope.launch {
            repository.updateUser(user).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _users.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _users.value = DataState.Success(it.data.data ?: mutableListOf())
                        _updateSuccess.value = EventWrapper(SuccessAction(true, ""))
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _users.value = DataState.Loading(loading = false)
                            _users.value = DataState.Error(it.exception)
                            _updateSuccess.value =
                                EventWrapper(SuccessAction(false, it.exception.message ?: ""))
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun deleteUser(id: String) {
        viewModelScope.launch {
            repository.deleteUser(id).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _users.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _users.value = DataState.Success(it.data.data ?: mutableListOf())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _users.value = DataState.Loading(loading = false)
                            _users.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }
}