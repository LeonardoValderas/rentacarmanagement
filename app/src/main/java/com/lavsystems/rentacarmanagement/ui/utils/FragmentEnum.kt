package com.lavsystems.rentacarmanagement.ui.utils

enum class FragmentEnum {
    CAR,
    BOOKING,
    USER
}