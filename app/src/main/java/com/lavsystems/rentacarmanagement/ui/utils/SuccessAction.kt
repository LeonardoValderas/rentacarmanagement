package com.lavsystems.rentacarmanagement.ui.utils

data class SuccessAction(
    val success: Boolean,
    val message: String
)