package com.lavsystems.rentacarmanagement.ui.utils

import com.lavsystems.rentacarmanagement.R
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
object CarStateReserved: CarState {
    override fun getState(): String {
        return CarState.RESERVED
    }

    override fun getColor(): Int {
        return R.color.red_reserved
    }
}