package com.lavsystems.rentacarmanagement.ui.utils

enum class TypeUserEnum(val code: Int) {
    ADMIN(1),
    NORMAL(2),
    READ(3);

    companion object {
        fun getTypeUser(code: Int) = values().first {
            it.code == code
        }
    }
}