package com.lavsystems.rentacarmanagement.ui.booking

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lavsystems.rentacarmanagement.data.api.utils.TokenException
import com.lavsystems.rentacarmanagement.data.dto.BookingDTO
import com.lavsystems.rentacarmanagement.data.repository.BookingRepository
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.Booking
import com.lavsystems.rentacarmanagement.model.BookingDetails
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import com.lavsystems.rentacarmanagement.ui.utils.EventWrapper
import com.lavsystems.rentacarmanagement.ui.utils.SuccessAction
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import java.util.*

class BookingViewModel(private val repository: BookingRepository) : ViewModel() {
    private val _bookingsDetails: MutableLiveData<DataState<MutableList<BookingDetails>>> =
        MutableLiveData(DataState.Idle)
    val bookingsDetails: LiveData<DataState<MutableList<BookingDetails>>> = _bookingsDetails

    private val _bookingDetails: MutableLiveData<DataState<BookingDetails>> =
        MutableLiveData(DataState.Idle)
    val bookingDetails: LiveData<DataState<BookingDetails>> = _bookingDetails

    private val _bookingDTO: MutableLiveData<DataState<BookingDTO>> =
        MutableLiveData(DataState.Idle)
    val bookingDTO: LiveData<DataState<BookingDTO>> = _bookingDTO

    private val _hasStartOrEndDateBooking = MutableLiveData<DataState<EventWrapper<Boolean>>>()
    val hasStartOrEndDateBooking: LiveData<DataState<EventWrapper<Boolean>>> = _hasStartOrEndDateBooking

    private val _sessionExpired = MutableLiveData<EventWrapper<Unit>>()
    val sessionExpired: LiveData<EventWrapper<Unit>> = _sessionExpired

    private val _saveSuccess = MutableLiveData<EventWrapper<SuccessAction>>()
    val saveSuccess: LiveData<EventWrapper<SuccessAction>> = _saveSuccess

    private val _updateSuccess = MutableLiveData<EventWrapper<SuccessAction>>()
    val updateSuccess: LiveData<EventWrapper<SuccessAction>> = _updateSuccess

    fun getBookingsByDate(date: String, onlyAvailable: Boolean){
        viewModelScope.launch {
            repository.getBookingsByDate(date, onlyAvailable).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _bookingsDetails.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _bookingsDetails.value = DataState.Success(it.data.data ?: mutableListOf())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _bookingsDetails.value = DataState.Loading(loading = false)
                            _bookingsDetails.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun hasStartOrEndDateBooking(id: String, carId: String, startDate: String, endDate: String){
        viewModelScope.launch {
            repository.hasStartOrEndDateBooking(id, carId, startDate, endDate).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _hasStartOrEndDateBooking.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _hasStartOrEndDateBooking.value = DataState.Success(EventWrapper(it.data.data ?: false))
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _hasStartOrEndDateBooking.value = DataState.Loading(loading = false)
                            _hasStartOrEndDateBooking.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun getFormBooking(id: String) {
        viewModelScope.launch {
            repository.getFormBooking(id).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _bookingDTO.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _bookingDTO.value = DataState.Success(it.data.data ?: BookingDTO())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _bookingDTO.value = DataState.Loading(loading = false)
                            _bookingDTO.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun saveBooking(date: String, onlyAvailable: Boolean, booking: Booking) {
        viewModelScope.launch {
            repository.saveBooking(date, onlyAvailable, booking).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _bookingsDetails.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _bookingsDetails.value = DataState.Success(it.data.data ?: mutableListOf())
                        _saveSuccess.value = EventWrapper(SuccessAction(true, ""))
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _bookingsDetails.value = DataState.Loading(loading = false)
                            _bookingsDetails.value = DataState.Error(it.exception)
                            _saveSuccess.value =
                                EventWrapper(SuccessAction(false, it.exception.message ?: ""))
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun updateBooking(date: String, onlyAvailable: Boolean, booking: Booking) {
        viewModelScope.launch {
            repository.updateBooking(date, onlyAvailable, booking).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _bookingsDetails.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _bookingsDetails.value = DataState.Success(it.data.data ?: mutableListOf())
                        _updateSuccess.value = EventWrapper(SuccessAction(true, ""))
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _bookingsDetails.value = DataState.Loading(loading = false)
                            _bookingsDetails.value = DataState.Error(it.exception)
                            _updateSuccess.value =
                                EventWrapper(SuccessAction(false, it.exception.message ?: ""))
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun deleteBooking(date: String, onlyAvailable: Boolean, id: String) {
        viewModelScope.launch {
            repository.deleteBooking(date, onlyAvailable, id).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _bookingsDetails.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _bookingsDetails.value = DataState.Success(it.data.data ?: mutableListOf())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _bookingsDetails.value = DataState.Loading(loading = false)
                            _bookingsDetails.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }
}