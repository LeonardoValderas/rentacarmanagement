package com.lavsystems.rentacarmanagement.ui.booking

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import androidx.core.util.Pair
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.lavsystems.rentacarmanagement.R
import com.lavsystems.rentacarmanagement.data.api.service.BookingService
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptorImpl
import com.lavsystems.rentacarmanagement.data.datasource.BookingDataSourceImpl
import com.lavsystems.rentacarmanagement.data.dto.BookingDTO
import com.lavsystems.rentacarmanagement.data.repository.BookingRepository
import com.lavsystems.rentacarmanagement.data.repository.BookingRepositoryImpl
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferencesImpl
import com.lavsystems.rentacarmanagement.databinding.FragmentBookingDialogBinding
import com.lavsystems.rentacarmanagement.model.Booking
import com.lavsystems.rentacarmanagement.ui.car.CarDialogFragment
import com.lavsystems.rentacarmanagement.ui.login.LoginActivity
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.getRangePicker
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.getTimePicker
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showToast
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import com.lavsystems.rentacarmanagement.ui.utils.EventWrapper
import java.lang.Exception

class BookingDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentBookingDialogBinding
    private lateinit var repository: BookingRepository
    private lateinit var viewModel: BookingViewModel
    private lateinit var carAdapter: CarAdapter
    private var booking = Booking()
    private var bookingId = ""
    private val isCreate: Boolean
        get() = bookingId.isEmpty()
    private lateinit var dateRangePicker: MaterialDatePicker<Pair<Long, Long>>
    private lateinit var deliveryTimePicker: MaterialTimePicker
    private lateinit var returnTimePicker: MaterialTimePicker
    private var selectedDate = ""
    private var onlyAvailable = true
    private var isUserOnlyRead = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen)
        repository = BookingRepositoryImpl(
            BookingDataSourceImpl(
                SharedPreferencesImpl(requireContext()), BookingService(
                    ConnectivityInterceptorImpl(requireContext()),
                    com.lavsystems.rentacarmanagement.BuildConfig.SERVER_URL
                )
            )
        )
        setUpViewModel()
        getBundleData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        return FragmentBookingDialogBinding.inflate(inflater, container, false).also {
            binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setWindowAnimations(
            R.style.DialogAnimation
        )
        setUpUI()
        if (isCreate)
            setUpPickers()
        setUpListener()
        setUpSpinnerAdapter()
        setUpObserverViewModel()
        setEnableByTypeUser()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getFormBooking(bookingId)
    }

    private fun setUpPickers(
        startDate: Long = MaterialDatePicker.todayInUtcMilliseconds(),
        endDate: Long = MaterialDatePicker.todayInUtcMilliseconds() + setThreeDays()
    ) {
        dateRangePicker = getRangePicker(
            SELECT_DATES,
            startDate,
            endDate
        )

        dateRangePicker.addOnPositiveButtonClickListener {
            booking.startDate = it.first
            booking.endDate = it.second
            //booking.startDate = it.first + setOneDays()
            // booking.endDate = it.second + setOneDays()
            with(binding) {
                tvDate.text = booking.startEndDate
                tvDelivery.text = booking.startDateTime
                tvReturn.text = booking.endDateTime
            }
        }

        val deliveryPairTime = getPairTime(booking.deliveryTime)
        deliveryTimePicker =
            getTimePicker(SELECT_HOUR, deliveryPairTime.first, deliveryPairTime.second)
        deliveryTimePicker.addOnPositiveButtonClickListener {
            booking.deliveryTime = getTimeFormat(true)
            binding.tvDelivery.text = booking.startDateTime
        }

        val returnPairTime = getPairTime(booking.returnTime)
        returnTimePicker = getTimePicker(SELECT_HOUR, returnPairTime.first, returnPairTime.second)
        returnTimePicker.addOnPositiveButtonClickListener {
            booking.returnTime = getTimeFormat(false)
            binding.tvReturn.text = booking.endDateTime
        }
    }

    private fun getPairTime(time: String): Pair<Int, Int> {
        if (time.isEmpty())
            return Pair(0, 0)
        time.split(":").let {
            return try {
                Pair(it[0].toInt(), it[1].toInt())
            } catch (e: Exception) {
                Pair(0, 0)
            }
        }
    }

    private fun setThreeDays() = setOneDays() * 3

    private fun setOneDays() = 86400000

    private fun getHour(isDelivery: Boolean) = if (isDelivery) {
        getHourFormat(deliveryTimePicker)
    } else {
        getHourFormat(returnTimePicker)
    }

    private fun getHourFormat(timePicker: MaterialTimePicker) =
        if (timePicker.hour.toString().length == 1)
            "0${timePicker.hour}" else
            timePicker.hour.toString()

    private fun getMinute(isDelivery: Boolean) = if (isDelivery) {
        getMinuteFormat(deliveryTimePicker)
    } else {
        getMinuteFormat(returnTimePicker)
    }

    private fun getMinuteFormat(timePicker: MaterialTimePicker) =
        if (timePicker.minute.toString().length == 1)
            "0${timePicker.minute}" else
            timePicker.minute.toString()

    private fun getTimeFormat(isDelivery: Boolean) =
        getHour(isDelivery).plus(":").plus(getMinute(isDelivery))

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            BookingViewModelFactory(repository)
        )[BookingViewModel::class.java]
    }

    private fun setUpObserverViewModel() {
        with(viewModel) {
            bookingDTO.observe(this@BookingDialogFragment) { state ->
                handleBookingUI(state)
            }

            hasStartOrEndDateBooking.observe(this@BookingDialogFragment) { state ->
                handleStartOrEndDateBookingUI(state)
//                success.getContentIfNotHandled()?.let {
//                    if (it.success) {
//                        cleanComponents()
//                        showToast(
//                            requireActivity(),
//                            requireActivity().getString(R.string.booking_added_success)
//                        )
//                    } else {
//                        showToast(
//                            requireActivity(),
//                            requireActivity().getString(R.string.booking_added_error, it.message)
//                        )
//                    }
//                }
            }
            saveSuccess.observe(this@BookingDialogFragment) { success ->
                success.getContentIfNotHandled()?.let {
                    if (it.success) {
                        cleanComponents()
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.booking_added_success)
                        )
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.booking_added_error, it.message)
                        )
                    }
                }
            }

            updateSuccess.observe(this@BookingDialogFragment) { success ->
                success.getContentIfNotHandled()?.let {
                    if (it.success) {
                        cleanComponents()
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.booking_updated_success)
                        )
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.booking_updated_error, it.message)
                        )
                    }
                }
            }

            sessionExpired.observe(requireActivity()) { event ->
                event.getContentIfNotHandled()?.let {
                    showToast(requireContext(), getString(R.string.expired_session))
                    goToLogin()
                }
            }
        }
    }

    private fun cleanComponents() {
        with(binding) {
            edDrivingLicence.setText(EMPTY_STRING)
            edFly.setText(EMPTY_STRING)
            edHotel.setText(EMPTY_STRING)
            edReturnPlace.setText(EMPTY_STRING)
            edPrice.setText(EMPTY_STRING)
            edCommission.setText(EMPTY_STRING)
            edComment.setText(EMPTY_STRING)
            btnActions.text = requireActivity().getString(R.string.create_title)
        }
    }

    private fun setEnableByTypeUser() {
        with(binding) {
            btnActions.visibility = if(isUserOnlyRead) View.INVISIBLE else View.VISIBLE
            edDrivingLicence.isEnabled = !isUserOnlyRead
            edFly.isEnabled = !isUserOnlyRead
            edHotel.isEnabled = !isUserOnlyRead
            edReturnPlace.isEnabled = !isUserOnlyRead
            edPrice.isEnabled = !isUserOnlyRead
            edCommission.isEnabled = !isUserOnlyRead
            edComment.isEnabled = !isUserOnlyRead
            edDeliveryPlace.isEnabled = !isUserOnlyRead
            spCar.isEnabled = !isUserOnlyRead
            llDate.isEnabled = !isUserOnlyRead
            llDelivery.isEnabled = !isUserOnlyRead
            llReturn.isEnabled = !isUserOnlyRead
        }
    }

    private fun setUpListener() {
        with(binding) {
            btnClosed.setOnClickListener {
                dismiss()
            }
            btnActions.setOnClickListener {
                if (setFlyToBooking() && setLicenceToBooking() && setHotelToBooking()) {
                    setCommentToBooking()
                    setPriceToBooking()
                    setCommissionToBooking()
                    setDeliveryCarToBooking()
                    setReturnCarToBooking()

                    if (!booking.isRequiredEmptyData) {
                        viewModel.hasStartOrEndDateBooking(
                            bookingId,
                            booking.car.id,
                            booking.startDate.toString(),
                            booking.endDate.toString()
                        )
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.required_datas_error)
                        )
                    }
                }
            }

            llDate.setOnClickListener {
                if (!dateRangePicker.isAdded)
                    dateRangePicker.show(childFragmentManager, DATA_RANGE_PICKER)
            }

            llDelivery.setOnClickListener {
                if (booking.startDate != null && !deliveryTimePicker.isAdded)
                    deliveryTimePicker.show(childFragmentManager, DELIVERY_TIME_PICKER)
            }

            llReturn.setOnClickListener {
                if (booking.endDate != null && !returnTimePicker.isAdded)
                    returnTimePicker.show(childFragmentManager, RETURN_TIME_PICKER)
            }
        }
    }

    private fun setDeliveryCarToBooking() {
        booking.deliveryPlace = binding.edDeliveryPlace.text.toString()
    }

    private fun setReturnCarToBooking() {
        booking.returnPlace = binding.edReturnPlace.text.toString()
    }

    private fun setFlyToBooking(): Boolean {
        binding.edFly.let { ed ->
            ed.text.toString().let {
                it.ifEmpty {
                    binding.edFly.error = requireActivity().getString(R.string.required_data_error)
                    return false
                }
                ed.error = null
                booking.fly = it
                return true
            }
        }
    }

    private fun setPriceToBooking() {
        booking.price = binding.edPrice.text.toString()
    }

    private fun setCommissionToBooking() {
        booking.commission = binding.edCommission.text.toString()
    }

    private fun setHotelToBooking(): Boolean {
        binding.edHotel.let { ed ->
            ed.text.toString().let {
                it.ifEmpty {
                    ed.error = requireActivity().getString(R.string.required_data_error)
                    return false
                }
                ed.error = null
                booking.hotel = it
                return true
            }
        }
    }

    private fun setLicenceToBooking(): Boolean {
        binding.edDrivingLicence.let { ed ->
            ed.text.toString().let {
                it.ifEmpty {
                    ed.error = requireActivity().getString(R.string.required_data_error)
                    return false
                }
                ed.error = null
                booking.drivingLicense = it
                return true
            }
        }
    }

    private fun setCommentToBooking() {
        booking.comment = binding.edComment.text.toString()
    }

    private fun getBundleData() {
        arguments?.let {
            bookingId = it.getString(BOOKING_ID_KEY, EMPTY_STRING)
            selectedDate = it.getString(BOOKING_DATE_KEY, EMPTY_STRING)
            onlyAvailable = it.getBoolean(BOOKING_ONLY_AVAILABLE_KEY, true)
            isUserOnlyRead = it.getBoolean(BOOKING_USER_TYPE_KEY, false)
        }
    }

    private fun setUpUI() {
        with(binding) {
            spCar.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        carAdapter.getCarForPosition(position).let {
                            booking.car = it
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
        }
    }

    private fun setUpSpinnerAdapter() {
        carAdapter = CarAdapter(requireActivity(), R.layout.simple_spinner_standar_item).also {
            binding.spCar.adapter = it
        }
    }

    private fun handleBookingUI(uiState: DataState<BookingDTO>) {
        when (uiState) {
            is DataState.Success<BookingDTO> -> {
                if (uiState.data.booking == null && !isCreate) {
                    handlerErrorVisibility(true, EMPTY_STRING)
                    handlerProgressBarVisibility(false)
                    handlerContainerVisibility(false)
                    return
                }

                carAdapter.setCars(uiState.data.cars)

                if (!isCreate) {
                    booking = uiState.data.booking!!
                    setBookingUI()
                }
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(true)
            }
            is DataState.Error -> {
                handlerErrorVisibility(true, uiState.throwable.message ?: EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(false)
            }
            is DataState.Loading -> {
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(true)
               // handlerContainerVisibility(false)
            }
            is DataState.Idle -> Unit
        }
    }

    private fun handleStartOrEndDateBookingUI(uiState: DataState<EventWrapper<Boolean>>) {
        when (uiState) {
            is DataState.Success<EventWrapper<Boolean>> -> {
                uiState.data.getContentIfNotHandled()?.let {
                    if (it) {
                        ComponentUtils.showDialog(
                            requireContext(),
                            getString(R.string.has_end_start_booking_message_dialog),
                            getString(
                                R.string.accept_title
                            ),
                            getString(R.string.cancel_title)
                        ) {
                            saveOrUpdateBooking()
                        }
                    } else {
                        saveOrUpdateBooking()
                    }
                }
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(true)
            }
            is DataState.Error -> {
                handlerErrorVisibility(true, uiState.throwable.message ?: EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(false)
            }
            is DataState.Loading -> {
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(true)
//                handlerContainerVisibility(false)
            }
            is DataState.Idle -> Unit
        }
    }

    private fun saveOrUpdateBooking() {
        if (isCreate)
            viewModel.saveBooking(selectedDate, onlyAvailable, booking)
        else
            viewModel.updateBooking(selectedDate, onlyAvailable, booking)
    }

    private fun setBookingUI() {
        with(binding) {
            btnActions.text = requireActivity().getText(R.string.update_title)
            spCar.setSelection(carAdapter.getPositionByCar(booking.car))

            if (booking.startDate != null && booking.endDate != null)
                setUpPickers(booking.startDate!!, booking.endDate!!)
            else
                setUpPickers()

            edDrivingLicence.setText(booking.drivingLicense)
            edFly.setText(booking.fly)
            edHotel.setText(booking.hotel)
            tvDate.text = booking.startEndDate
            tvDelivery.text = booking.startDateTime
            edDeliveryPlace.setText(booking.deliveryPlace)
            tvReturn.text = booking.endDateTime
            edReturnPlace.setText(booking.returnPlace)
            edComment.setText(booking.comment)
            edPrice.setText(booking.price)
            edCommission.setText(booking.commission)
        }
    }

    private fun handlerProgressBarVisibility(show: Boolean) {
        with(binding) {
            iProgressBar.progressBar.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerContainerVisibility(show: Boolean) {
        with(binding) {
            clContainer.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerErrorVisibility(show: Boolean, error: String) {
        with(binding) {
            iGenericError.clGenericError.visibility = if (show) View.VISIBLE else View.GONE
            iGenericError.tvMessage.text = error
            iGenericError.btnClose.setOnClickListener {
                iGenericError.clGenericError.visibility = View.GONE
                dismiss()
            }
        }
    }

    private fun goToLogin(){
        startActivity(Intent(requireContext(), LoginActivity::class.java))
        requireActivity().finish()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
    }

    companion object {
        fun newInstance(id: String, date: String, onlyAvailable: Boolean, isOnlyRead: Boolean): BookingDialogFragment {
            Bundle().apply {
                putString(BOOKING_ID_KEY, id)
                putString(BOOKING_DATE_KEY, date)
                putBoolean(BOOKING_ONLY_AVAILABLE_KEY, onlyAvailable)
                putBoolean(BOOKING_USER_TYPE_KEY, isOnlyRead)
            }.also { b ->
                return BookingDialogFragment().apply {
                    arguments = b
                }
            }
        }

        const val BOOKING_ID_KEY = "booking_id_key"
        const val BOOKING_DATE_KEY = "booking_date_key"
        const val BOOKING_ONLY_AVAILABLE_KEY = "booking_only_available_key"
        const val BOOKING_USER_TYPE_KEY = "booking_user_type_key"

        const val BOOKING_DIALOG_FRAGMENT_FLAG = "booking_dialog_fragment_flag"
        const val EMPTY_STRING = ""
        const val DATA_RANGE_PICKER = "date_range_picker"
        const val DELIVERY_TIME_PICKER = "delivery_time_picker"
        const val RETURN_TIME_PICKER = "return_time_picker"
        const val SELECT_DATES = "Selecciona las fechas"
        const val SELECT_HOUR = "Selecciona la hora"
    }
}