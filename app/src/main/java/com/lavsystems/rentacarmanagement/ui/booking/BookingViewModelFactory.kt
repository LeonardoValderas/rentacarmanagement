package com.lavsystems.rentacarmanagement.ui.booking

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lavsystems.rentacarmanagement.data.repository.BookingRepository

class BookingViewModelFactory(private val repository: BookingRepository): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(BookingRepository::class.java)
            .newInstance(repository)
    }
}