package com.lavsystems.rentacarmanagement.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lavsystems.rentacarmanagement.model.User
import com.lavsystems.rentacarmanagement.ui.utils.EventWrapper
import com.lavsystems.rentacarmanagement.ui.utils.TypeUserEnum
import java.lang.NullPointerException

class CommunicationViewModel : ViewModel() {

    private val _searchText = MutableLiveData<String>()
    val searchText: LiveData<String> = _searchText

    private val _isCreateCar = MutableLiveData<EventWrapper<Boolean>>()
    val isCreateCar: LiveData<EventWrapper<Boolean>> = _isCreateCar

    private val _isCreateBooking = MutableLiveData<EventWrapper<Boolean>>()
    val isCreateBooking: LiveData<EventWrapper<Boolean>> = _isCreateBooking

    private val _isCreateUser = MutableLiveData<EventWrapper<Boolean>>()
    val isCreateUser: LiveData<EventWrapper<Boolean>> = _isCreateUser

    var user: User? = null

    fun setSearchText(text: String){
        _searchText.value = text
    }

    fun createCar(){
        _isCreateCar.value = EventWrapper(true)
    }

    fun createBooking(){
        _isCreateBooking.value = EventWrapper(true)
    }

    fun createUser(){
        _isCreateUser.value = EventWrapper(true)
    }

    fun isAdmin(): Boolean{
        if(user == null)
            throw NullPointerException()
        return user?.type?.code == TypeUserEnum.ADMIN.code
    }

    fun isOnlyRead(): Boolean{
        if(user == null)
            throw NullPointerException()
        return user?.type?.code == TypeUserEnum.READ.code
    }
}