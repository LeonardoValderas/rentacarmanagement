package com.lavsystems.rentacarmanagement.ui.car

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lavsystems.rentacarmanagement.data.api.utils.TokenException
import com.lavsystems.rentacarmanagement.data.dto.CarDTO
import com.lavsystems.rentacarmanagement.data.repository.CarRepository
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.Car
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import com.lavsystems.rentacarmanagement.ui.utils.EventWrapper
import com.lavsystems.rentacarmanagement.ui.utils.SuccessAction
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class CarViewModel(private val repository: CarRepository) : ViewModel() {

    private val _cars: MutableLiveData<DataState<MutableList<Car>>> =
        MutableLiveData(DataState.Idle)
    val cars: LiveData<DataState<MutableList<Car>>> = _cars

    private val _carDTO: MutableLiveData<DataState<CarDTO>> =
        MutableLiveData(DataState.Idle)
    val carDTO: LiveData<DataState<CarDTO>> = _carDTO

    private val _sessionExpired = MutableLiveData<EventWrapper<Unit>>()
    val sessionExpired: LiveData<EventWrapper<Unit>> = _sessionExpired

    private val _saveSuccess = MutableLiveData<EventWrapper<SuccessAction>>()
    val saveSuccess: LiveData<EventWrapper<SuccessAction>> = _saveSuccess

    private val _updateSuccess = MutableLiveData<EventWrapper<SuccessAction>>()
    val updateSuccess: LiveData<EventWrapper<SuccessAction>> = _updateSuccess

    fun getCars() {
        viewModelScope.launch {
            repository.getCars().onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _cars.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _cars.value = DataState.Success(it.data.data ?: mutableListOf())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _cars.value = DataState.Loading(loading = false)
                            _cars.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun getFormCar(id: String) {
        viewModelScope.launch {
            repository.getFormCar(id).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _carDTO.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _carDTO.value = DataState.Success(it.data.data ?: CarDTO())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _carDTO.value = DataState.Loading(loading = false)
                            _carDTO.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun saveCar(car: Car) {
        viewModelScope.launch {
            repository.saveCar(car).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _cars.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _cars.value = DataState.Success(it.data.data ?: mutableListOf())
                        _saveSuccess.value = EventWrapper(SuccessAction(true, ""))
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _cars.value = DataState.Loading(loading = false)
                            _cars.value = DataState.Error(it.exception)
                            _saveSuccess.value =
                                EventWrapper(SuccessAction(false, it.exception.message ?: ""))
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun updateCar(car: Car) {
        viewModelScope.launch {
            repository.updateCar(car).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _cars.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _cars.value = DataState.Success(it.data.data ?: mutableListOf())
                        _updateSuccess.value = EventWrapper(SuccessAction(true, ""))
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _cars.value = DataState.Loading(loading = false)
                            _cars.value = DataState.Error(it.exception)
                            _updateSuccess.value =
                                EventWrapper(SuccessAction(false, it.exception.message ?: ""))
                        }
                    }
                }
            }.launchIn(this)
        }
    }

    fun deleteCar(id: String) {
        viewModelScope.launch {
            repository.deleteCar(id).onEach {
                when (it) {
                    is Response.NotInitialized, Response.Loading -> {
                        _cars.value = DataState.Loading(loading = true)
                    }
                    is Response.Success -> {
                        _cars.value = DataState.Success(it.data.data ?: mutableListOf())
                    }
                    is Response.Error -> {
                        if(it.exception is TokenException){
                            _sessionExpired.value = EventWrapper(Unit)
                        } else {
                            _cars.value = DataState.Loading(loading = false)
                            _cars.value = DataState.Error(it.exception)
                        }
                    }
                }
            }.launchIn(this)
        }
    }
}