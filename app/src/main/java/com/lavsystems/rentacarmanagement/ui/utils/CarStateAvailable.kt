package com.lavsystems.rentacarmanagement.ui.utils

import com.lavsystems.rentacarmanagement.R
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
object CarStateAvailable: CarState {
    override fun getState(): String {
        return CarState.AVAILABLE
    }

    override fun getColor(): Int {
        return R.color.teal_700
    }
}