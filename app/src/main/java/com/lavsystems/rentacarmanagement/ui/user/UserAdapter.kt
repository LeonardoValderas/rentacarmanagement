package com.lavsystems.rentacarmanagement.ui.user

import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.lavsystems.rentacarmanagement.R
import com.lavsystems.rentacarmanagement.databinding.ItemUserBinding
import com.lavsystems.rentacarmanagement.model.User
import com.lavsystems.rentacarmanagement.ui.utils.RecyclerListener
import com.lavsystems.rentacarmanagement.ui.utils.TypeUserEnum

class UserAdapter(private val listener: RecyclerListener, private val isOnlyRead: Boolean) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    private var originUsers: MutableList<User> = mutableListOf()
    private var users: MutableList<User> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserAdapter.UserViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return UserViewHolder(layoutInflater.inflate(R.layout.item_user, parent, false), listener, isOnlyRead)
    }

    override fun onBindViewHolder(holderCar: UserViewHolder, position: Int) {
        val item = users[position]
        holderCar.bind(item)
    }

    fun getItemByPosition(position: Int) = users[position]

    fun filterByBrand(text: String) {
        users = if (text.isEmpty())
            originUsers
        else
            originUsers.filter { it.userName.contains(text) }.toMutableList()
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = users.size

    fun setUsers(users: MutableList<User>) {
        this.originUsers = users
        this.users = users
        notifyDataSetChanged()
    }

    inner class UserViewHolder(view: View, private val listener: RecyclerListener, private val isOnlyRead: Boolean) :
        RecyclerView.ViewHolder(view), PopupMenu.OnMenuItemClickListener {
        private val binding = ItemUserBinding.bind(view)

        fun bind(user: User) {
            with(binding) {
                root.setOnClickListener {
                    showPopupMenu(it)
                }
                tvName.text = user.userName
                tvPass.text = user.password
                tvType.text = user.type.name
            }
        }

        private fun showPopupMenu(view: View) {
            PopupMenu(binding.root.context, view).apply {
                inflate(R.menu.menu_popup)
                setOnMenuItemClickListener(this@UserViewHolder)
                if(isOnlyRead) {
                    menu.findItem(R.id.action_popup_show).isVisible = true
                    menu.findItem(R.id.action_popup_edit).isVisible = false
                    menu.findItem(R.id.action_popup_delete).isVisible = false
                } else {
                    menu.findItem(R.id.action_popup_show).isVisible = false
                    menu.findItem(R.id.action_popup_edit).isVisible = true
                    menu.findItem(R.id.action_popup_delete).isVisible = true
                }
                show()
            }
        }

        override fun onMenuItemClick(item: MenuItem?): Boolean {
            return when (item?.itemId) {
                R.id.action_popup_show -> {
                    listener.onClick(getItemByPosition(absoluteAdapterPosition).id)
                    true
                }
                R.id.action_popup_edit -> {
                    listener.onMenuClickEdit(getItemByPosition(absoluteAdapterPosition).id)
                    true
                }
                R.id.action_popup_delete -> {
                    listener.onMenuClickDelete(getItemByPosition(absoluteAdapterPosition).id)
                    true
                }
                else -> false
            }
        }
    }
}