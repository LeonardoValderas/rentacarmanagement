package com.lavsystems.rentacarmanagement.ui.user

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.lavsystems.rentacarmanagement.R
import com.lavsystems.rentacarmanagement.data.api.service.UserService
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptorImpl
import com.lavsystems.rentacarmanagement.data.datasource.UserDataSourceImpl
import com.lavsystems.rentacarmanagement.data.repository.UserRepository
import com.lavsystems.rentacarmanagement.data.repository.UserRepositoryImpl
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferencesImpl
import com.lavsystems.rentacarmanagement.databinding.FragmentUserBinding
import com.lavsystems.rentacarmanagement.model.User
import com.lavsystems.rentacarmanagement.ui.home.CommunicationViewModel
import com.lavsystems.rentacarmanagement.ui.login.LoginActivity
import com.lavsystems.rentacarmanagement.ui.user.UserDialogFragment.Companion.USER_DIALOG_FRAGMENT_FLAG
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showDialog
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showToast
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import com.lavsystems.rentacarmanagement.ui.utils.RecyclerListener
import java.lang.NullPointerException

class UserFragment : Fragment(), RecyclerListener {
    private lateinit var binding: FragmentUserBinding
    private lateinit var userAdapter: UserAdapter
    private lateinit var repository: UserRepository
    private lateinit var viewModel: UserViewModel
    private val communicationViewModel: CommunicationViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        repository = UserRepositoryImpl(
            UserDataSourceImpl(
                SharedPreferencesImpl(requireContext()), UserService(
                    ConnectivityInterceptorImpl(requireContext()),
                    com.lavsystems.rentacarmanagement.BuildConfig.SERVER_URL
                )
            )
        )
        setUpViewModel()
        setUpObserverViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRecyclerView()
        initAdView()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getUsers()
        binding.avUser.resume()
    }

    override fun onPause() {
        binding.avUser.pause()
        super.onPause()
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            UserViewModelFactory(repository)
        )[UserViewModel::class.java]
    }

    private fun setUpObserverViewModel() {
        with(viewModel) {
            users.observe(this@UserFragment) { state ->
                handleUiUsers(state)
            }

            sessionExpired.observe(requireActivity()) { event ->
                event.getContentIfNotHandled()?.let {
                    showToast(requireContext(), getString(R.string.expired_session))
                    goToLogin()
                }
            }
        }
        with(communicationViewModel) {
            searchText.observe(this@UserFragment) { text ->
                userAdapter.filterByBrand(text)
            }

            isCreateUser.observe(this@UserFragment) { isCreate ->
                isCreate.getContentIfNotHandled()?.let {
                    if (it) {
                        openUserFragmentDialog()
                    }
                }
            }
        }
    }

    private fun initAdView() {
        binding.avUser.loadAd(AdRequest.Builder().build())
    }

    private fun initRecyclerView() {
        userAdapter = UserAdapter(this, communicationViewModel.isOnlyRead())
        val linearLayoutManager = getLinearLayoutManager()
        with(binding.rvUser) {
            layoutManager = linearLayoutManager
            adapter = userAdapter
            addItemDecoration(getDividerItemDecoration(linearLayoutManager))
        }
    }

    private fun getLinearLayoutManager() = LinearLayoutManager(requireContext())

    private fun getDividerItemDecoration(linearLayoutManager: LinearLayoutManager) =
        DividerItemDecoration(
            requireContext(),
            linearLayoutManager.orientation
        )

    private fun handleUiUsers(uiState: DataState<MutableList<User>>) {
        when (uiState) {
            is DataState.Success<MutableList<User>> -> {
                userAdapter.setUsers(uiState.data)
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerRecyclerVisibility(true)
            }
            is DataState.Error -> {
                handlerErrorVisibility(true, uiState.throwable.message ?: EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerRecyclerVisibility(false)
            }
            is DataState.Loading -> {
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(true)
                handlerRecyclerVisibility(false)
            }
            is DataState.Idle -> Unit
        }
    }

    private fun handlerProgressBarVisibility(show: Boolean) {
        with(binding) {
            iProgressBar.progressBar.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerRecyclerVisibility(show: Boolean) {
        with(binding) {
            rvUser.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerErrorVisibility(show: Boolean, error: String) {
        with(binding) {
            iGenericError.clGenericError.visibility = if (show) View.VISIBLE else View.GONE
            iGenericError.tvMessage.text = error
            iGenericError.btnClose.setOnClickListener {
                iGenericError.clGenericError.visibility = View.GONE
                viewModel.getUsers()
            }
        }
    }

    override fun onClick(id: String) {
        openUserFragmentDialog(id)
    }

    override fun onMenuClickEdit(id: String) {
        openUserFragmentDialog(id)
    }

    override fun onMenuClickDelete(id: String) {
        showDialog(
            requireContext(), getString(R.string.delete_user_message_dialog), getString(
                R.string.accept_title
            ), getString(R.string.cancel_title)
        ) {
            deleteUser(id)
        }
    }

    private fun deleteUser(id: String) {
        try {
            if (communicationViewModel.isAdmin())
                viewModel.deleteUser(id)
            else
                showToast(requireContext(), requireContext().getString(R.string.user_without_permissions))
        } catch (e: NullPointerException) {
            showToast(requireContext(), requireContext().getString(R.string.unknown_error_text))
        }
    }

    private fun openUserFragmentDialog(id: String = "") {
        UserDialogFragment.newInstance(id).show(parentFragmentManager, USER_DIALOG_FRAGMENT_FLAG)
    }

    private fun goToLogin(){
        startActivity(Intent(requireContext(), LoginActivity::class.java))
        requireActivity().finish()
    }

    override fun onDestroy() {
        binding.avUser.destroy()
        super.onDestroy()
    }

    companion object {
        const val EMPTY_STRING = ""
    }
}
