package com.lavsystems.rentacarmanagement.ui.booking

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.gms.ads.AdRequest
import com.google.android.material.datepicker.MaterialDatePicker
import com.lavsystems.rentacarmanagement.R
import com.lavsystems.rentacarmanagement.data.api.service.BookingService
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptorImpl
import com.lavsystems.rentacarmanagement.data.datasource.BookingDataSourceImpl
import com.lavsystems.rentacarmanagement.data.repository.BookingRepository
import com.lavsystems.rentacarmanagement.data.repository.BookingRepositoryImpl
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferencesImpl
import com.lavsystems.rentacarmanagement.databinding.FragmentBookingBinding
import com.lavsystems.rentacarmanagement.model.BookingDetails
import com.lavsystems.rentacarmanagement.ui.home.CommunicationViewModel
import com.lavsystems.rentacarmanagement.ui.login.LoginActivity
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.getDatePicker
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showDialog
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showToast
import com.lavsystems.rentacarmanagement.ui.utils.DataState
import com.lavsystems.rentacarmanagement.ui.utils.DateTimeUtils.dateShortFormatString
import com.lavsystems.rentacarmanagement.ui.utils.RecyclerListener
import com.lavsystems.rentacarmanagement.ui.utils.TypeUserEnum
import java.util.*

class BookingFragment : Fragment(), RecyclerListener {

    private lateinit var binding: FragmentBookingBinding
    private lateinit var bookingDetailsAdapter: BookingDetailsAdapter
    private lateinit var repository: BookingRepository
    private lateinit var viewModel: BookingViewModel
    private val communicationViewModel: CommunicationViewModel by activityViewModels()
    private var bookingsOrigin = mutableListOf<BookingDetails>()
    private var bookings = mutableListOf<BookingDetails>()
    private lateinit var datePicker: MaterialDatePicker<Long>
    private var selectedDate = Date().time - setOneDays()
    private var onlyAvailable = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        repository = BookingRepositoryImpl(
            BookingDataSourceImpl(
                SharedPreferencesImpl(requireContext()), BookingService(
                    ConnectivityInterceptorImpl(requireContext()),
                    com.lavsystems.rentacarmanagement.BuildConfig.SERVER_URL
                )
            )
        )
        setUpViewModel()
        setUpObserverViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentBookingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpListener()
        setUpPickers()
        initAdView()
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            BookingViewModelFactory(repository)
        )[BookingViewModel::class.java]
    }

    override fun onResume() {
        super.onResume()
        initRecyclerView()
        getBookingsByDate()
        setDateText()
        binding.avBooking.resume()
    }

    override fun onPause() {
        binding.avBooking.pause()
        super.onPause()
    }

    private fun setUpObserverViewModel() {
        with(viewModel) {
            bookingsDetails.observe(requireActivity()) { state ->
                handleUiBookings(state)
            }
            sessionExpired.observe(requireActivity()) { event ->
                event.getContentIfNotHandled()?.let {
                    showToast(requireContext(), getString(R.string.expired_session))
                    goToLogin()
                }
            }
        }
        with(communicationViewModel) {
            searchText.observe(requireActivity()) { text ->
                bookings = if (text.isEmpty())
                    bookingsOrigin
                else
                    bookingsOrigin.filter { it.car.brand.name.contains(text) }.toMutableList()
                bookingDetailsAdapter.addNewList(bookings as MutableList<Any>)
            }

            isCreateBooking.observe(requireActivity()) { isCreate ->
                isCreate.getContentIfNotHandled()?.let {
                    if (it) {
                        openBookingFragmentDialog()
                    }
                }
            }
        }
    }

    private fun initAdView() {
        binding.avBooking.loadAd(AdRequest.Builder().build())
    }

    private fun setUpListener() {
        binding.cbBooking.setOnCheckedChangeListener { _, isChecked ->
            onlyAvailable = !isChecked
            getBookingsByDate(!isChecked)
        }
        binding.fabDate.setOnClickListener {
            if (!datePicker.isAdded)
                datePicker.show(childFragmentManager, DATE_PICKER)
        }
    }

    private fun getBookingsByDate(onlyAvailable: Boolean = !binding.cbBooking.isChecked) {
        viewModel.getBookingsByDate(selectedDate.toString(), onlyAvailable)
    }

    private fun setDateText() {
        binding.tvDateBooking.text = dateShortFormatString(selectedDate + setOneDays())
    }

    private fun initRecyclerView() {
        bookingDetailsAdapter =
            BookingDetailsAdapter(mutableListOf(), this, communicationViewModel.isOnlyRead())
        val linearLayoutManager = getLinearLayoutManager()
        with(binding.rvBooking) {
            layoutManager = linearLayoutManager
            adapter = bookingDetailsAdapter
            //addItemDecoration(getDividerItemDecoration(linearLayoutManager))
        }
    }

    private fun setUpPickers(
        startDate: Long = MaterialDatePicker.todayInUtcMilliseconds()
    ) {
        datePicker = getDatePicker(
            SELECT_DATE,
            startDate
        )

        datePicker.addOnPositiveButtonClickListener {
            selectedDate = it //+ //setOneDays()
            setDateText()
            getBookingsByDate()
        }
    }

    private fun setOneDays() = 86400000

    private fun getLinearLayoutManager() = LinearLayoutManager(requireContext())

    private fun getDividerItemDecoration(linearLayoutManager: LinearLayoutManager) =
        DividerItemDecoration(
            requireContext(),
            linearLayoutManager.orientation
        )

    private fun handleUiBookings(uiState: DataState<MutableList<BookingDetails>>) {
        when (uiState) {
            is DataState.Success<MutableList<BookingDetails>> -> {
                if (uiState.data.isEmpty()) {
                    handlerEmptyVisibility(true)
                    handlerRecyclerVisibility(false)
                } else {
                    bookingsOrigin = uiState.data
                    bookings = uiState.data
                    bookingDetailsAdapter.addNewList(bookingsOrigin as MutableList<Any>)
                    handlerEmptyVisibility(false)
                    handlerRecyclerVisibility(true)
                }
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(false)
            }
            is DataState.Error -> {
                handlerErrorVisibility(true, uiState.throwable.message ?: EMPTY_STRING)
                handlerEmptyVisibility(false)
                handlerProgressBarVisibility(false)
                handlerRecyclerVisibility(false)
            }
            is DataState.Loading -> {
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerEmptyVisibility(false)
                handlerProgressBarVisibility(true)
                handlerRecyclerVisibility(false)
            }
            is DataState.Idle -> Unit
        }
    }

    private fun handlerProgressBarVisibility(show: Boolean) {
        with(binding) {
            iProgressBar.progressBar.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerRecyclerVisibility(show: Boolean) {
        binding.rvBooking.visibility = if (show) View.VISIBLE else View.GONE
    }

    private fun handlerErrorVisibility(show: Boolean, error: String) {
        with(binding) {
            iGenericError.clGenericError.visibility = if (show) View.VISIBLE else View.GONE
            iGenericError.tvMessage.text = error
            iGenericError.btnClose.setOnClickListener {
                iGenericError.clGenericError.visibility = View.GONE
                getBookingsByDate()
            }
        }
    }

    private fun handlerEmptyVisibility(show: Boolean) {
        binding.tvBookingEmpty.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onClick(id: String) {
        openBookingFragmentDialog(id)
    }

    override fun onMenuClickEdit(id: String) {
        openBookingFragmentDialog(id)
    }

    override fun onMenuClickDelete(id: String) {
        showDialog(
            requireContext(), getString(R.string.delete_booking_message_dialog), getString(
                R.string.accept_title
            ), getString(R.string.cancel_title)
        ) {
            deleteBooking(id)
        }
    }

    private fun deleteBooking(id: String) {
        viewModel.deleteBooking(selectedDate.toString(), onlyAvailable, id)
    }

    private fun openBookingFragmentDialog(id: String = "") {
        BookingDialogFragment.newInstance(
            id,
            selectedDate.toString(),
            onlyAvailable,
            communicationViewModel.user?.type?.code == TypeUserEnum.READ.code
        ).show(
            parentFragmentManager,
            BookingDialogFragment.BOOKING_DIALOG_FRAGMENT_FLAG
        )
    }

    private fun goToLogin() {
        startActivity(Intent(requireContext(), LoginActivity::class.java))
        requireActivity().finish()
    }

    override fun onDestroy() {
        binding.avBooking.destroy()
        super.onDestroy()
    }

    companion object {
        const val SELECT_DATE = "Selecciona fecha de inicio"
        const val DATE_PICKER = "date_picker"
        const val EMPTY_STRING = ""
    }
}