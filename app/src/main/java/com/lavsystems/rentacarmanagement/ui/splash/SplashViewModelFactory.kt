package com.lavsystems.rentacarmanagement.ui.splash

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.lavsystems.rentacarmanagement.data.repository.SplashRepository

class SplashViewModelFactory(private val repository: SplashRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(SplashRepository::class.java)
            .newInstance(repository)
    }
}