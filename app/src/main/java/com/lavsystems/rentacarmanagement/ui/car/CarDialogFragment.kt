package com.lavsystems.rentacarmanagement.ui.car

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.lavsystems.rentacarmanagement.R
import com.lavsystems.rentacarmanagement.data.api.service.CarService
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptorImpl
import com.lavsystems.rentacarmanagement.data.datasource.CarDataSourceImpl
import com.lavsystems.rentacarmanagement.data.dto.CarDTO
import com.lavsystems.rentacarmanagement.data.repository.CarRepository
import com.lavsystems.rentacarmanagement.data.repository.CarRepositoryImpl
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferencesImpl
import com.lavsystems.rentacarmanagement.databinding.FragmentCarDialogBinding
import com.lavsystems.rentacarmanagement.model.Car
import com.lavsystems.rentacarmanagement.ui.login.LoginActivity
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showToast
import com.lavsystems.rentacarmanagement.ui.utils.DataState

class CarDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentCarDialogBinding
    private lateinit var repository: CarRepository
    private lateinit var viewModel: CarViewModel
    private lateinit var brandAdapter: BrandAdapter
    private lateinit var colorAdapter: ColorAdapter
    private var car = Car()
    private var carId = ""
    private var isUserOnlyRead = false
    private val isCreate: Boolean
        get() = carId.isEmpty()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen)
        repository = CarRepositoryImpl(
            CarDataSourceImpl(
                SharedPreferencesImpl(requireContext()), CarService(
                    ConnectivityInterceptorImpl(requireContext()), com.lavsystems.rentacarmanagement.BuildConfig.SERVER_URL
                )
            )
        )
        setUpViewModel()
        getBundleData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        return FragmentCarDialogBinding.inflate(inflater, container, false).also {
            binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setWindowAnimations(
            R.style.DialogAnimation
        )

        setUpUI()
        setUpListener()
        setUpSpinnerAdapter()
        setUpObserverViewModel()
        setEnableByTypeUser()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getFormCar(carId)
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            CarViewModelFactory(repository)
        )[CarViewModel::class.java]
    }

    private fun setUpObserverViewModel() {
        with(viewModel) {
            carDTO.observe(this@CarDialogFragment) { state ->
                handleUiCar(state)
            }
            saveSuccess.observe(this@CarDialogFragment) { success ->
                success.getContentIfNotHandled()?.let {
                    if (it.success) {
                        cleanComponents()
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.car_added_success)
                        )
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.car_added_error, it.message)
                        )
                    }
                }
            }

            updateSuccess.observe(this@CarDialogFragment) { success ->
                success.getContentIfNotHandled()?.let {
                    if (it.success) {
                        cleanComponents()
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.car_updated_success)
                        )
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.car_updated_error, it.message)
                        )
                    }
                }
            }

            sessionExpired.observe(requireActivity()) { event ->
                event.getContentIfNotHandled()?.let {
                    showToast(requireContext(), getString(R.string.expired_session))
                    goToLogin()
                }
            }
        }
    }

    private fun cleanComponents() {
        with(binding) {
            edModel.setText(EMPTY_STRING)
            edPatent.setText(EMPTY_STRING)
            edComment.setText(EMPTY_STRING)
            btnActions.text = requireActivity().getString(R.string.create_title)
        }
    }

    private fun setUpListener() {
        with(binding) {
            btnClosed.setOnClickListener {
                dismiss()
            }
            btnActions.setOnClickListener {
                setModelToCar()
                setPatentToCar()
                setCommentToCar()
                if (!car.isRequiredEmptyData) {
                    if (isCreate)
                        viewModel.saveCar(car)
                    else
                        viewModel.updateCar(car)
                } else {
                    showToast(
                        requireActivity(),
                        requireActivity().getString(R.string.required_datas_error)
                    )
                }
            }
        }
    }

    private fun setModelToCar() {
        binding.edModel.let { ed ->
            ed.text.toString().let {
                it.ifEmpty {
                    ed.error = requireActivity().getString(R.string.required_data_error)
                    return@let
                }
                ed.error = null
                car.model = it
            }
        }
    }

    private fun setPatentToCar() {
        binding.edPatent.let { ed ->
            ed.text.toString().let {
                it.ifEmpty {
                    ed.error = requireActivity().getString(R.string.required_data_error)
                    return@let
                }
                ed.error = null
                car.patent = it
            }
        }
    }

    private fun setCommentToCar() {
        binding.edComment.let { ed ->
            ed.text.toString().let {
                car.comment = it
            }
        }
    }

    private fun getBundleData() {
        arguments?.let {
            carId = it.getString(CAR_ID_KEY, EMPTY_STRING)
            isUserOnlyRead = it.getBoolean(CAR_USER_TYPE_KEY, false)
        }
    }

    private fun setUpUI() {
        with(binding) {
            spBrand.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        brandAdapter.getBrandForPosition(position).let {
                            car.brand = it
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
            spColor.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        colorAdapter.getColorForPosition(position).let {
                            car.color = it
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
        }
    }

    private fun setUpSpinnerAdapter() {
        brandAdapter = BrandAdapter(requireActivity(), R.layout.simple_spinner_standar_item).also {
            binding.spBrand.adapter = it
        }
        colorAdapter = ColorAdapter(requireActivity(), R.layout.simple_spinner_standar_item).also {
            binding.spColor.adapter = it
        }
    }

    private fun handleUiCar(uiState: DataState<CarDTO>) {
        when (uiState) {
            is DataState.Success<CarDTO> -> {
                if (uiState.data.car == null && !isCreate) {
                    handlerErrorVisibility(true, EMPTY_STRING)
                    handlerProgressBarVisibility(false)
                    handlerContainerVisibility(false)
                    return
                }

                brandAdapter.setBrands(uiState.data.brands)
                colorAdapter.setColors(uiState.data.colors)
                if (!isCreate) {
                    car = uiState.data.car!!
                    setCarUI()
                }
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(true)
            }
            is DataState.Error -> {
                handlerErrorVisibility(true, uiState.throwable.message ?: EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(false)
            }
            is DataState.Loading -> {
                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(true)
                //handlerContainerVisibility(false)
            }
            is DataState.Idle -> Unit
        }
    }

    private fun setCarUI() {
        with(binding) {
            btnActions.text = requireActivity().getText(R.string.update_title)
            spBrand.setSelection(brandAdapter.getPositionByBrand(car.brand))
            spColor.setSelection(colorAdapter.getPositionByColor(car.color))
            edModel.setText(car.model)
            edPatent.setText(car.patent)
            edComment.setText(car.comment)
        }
    }

    private fun setEnableByTypeUser() {
        with(binding) {
            btnActions.visibility = if(isUserOnlyRead) View.INVISIBLE else View.VISIBLE
            spBrand.isEnabled = !isUserOnlyRead
            spColor.isEnabled = !isUserOnlyRead
            edModel.isEnabled = !isUserOnlyRead
            edPatent.isEnabled = !isUserOnlyRead
            edComment.isEnabled = !isUserOnlyRead
        }
    }

    private fun handlerProgressBarVisibility(show: Boolean) {
        with(binding) {
            iProgressBar.progressBar.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerContainerVisibility(show: Boolean) {
        with(binding) {
            clContainer.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerErrorVisibility(show: Boolean, error: String) {
        with(binding) {
            iGenericError.clGenericError.visibility = if (show) View.VISIBLE else View.GONE
            iGenericError.tvMessage.text = error
            iGenericError.btnClose.setOnClickListener {
                iGenericError.clGenericError.visibility = View.GONE
                dismiss()
            }
        }
    }

    private fun goToLogin(){
        startActivity(Intent(requireContext(), LoginActivity::class.java))
        requireActivity().finish()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
    }

    companion object {
        fun newInstance(id: String, isOnlyRead: Boolean): CarDialogFragment {
            Bundle().apply {
                putString(CAR_ID_KEY, id)
                putBoolean(CAR_USER_TYPE_KEY, isOnlyRead)
            }.also { b ->
                return CarDialogFragment().apply {
                    arguments = b
                }
            }
        }

        const val CAR_ID_KEY = "car_id_key"
        const val CAR_USER_TYPE_KEY = "car_user_type_key"
        const val CAR_DIALOG_FRAGMENT_FLAG = "car_dialog_fragment_flag"
        const val EMPTY_STRING = ""
    }
}