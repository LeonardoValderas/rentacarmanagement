package com.lavsystems.rentacarmanagement.ui.user

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.AdapterView
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.lavsystems.rentacarmanagement.R
import com.lavsystems.rentacarmanagement.data.api.service.UserService
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptorImpl
import com.lavsystems.rentacarmanagement.data.datasource.UserDataSourceImpl
import com.lavsystems.rentacarmanagement.data.dto.UserDTO
import com.lavsystems.rentacarmanagement.data.repository.UserRepository
import com.lavsystems.rentacarmanagement.data.repository.UserRepositoryImpl
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferencesImpl
import com.lavsystems.rentacarmanagement.databinding.FragmentUserDialogBinding
import com.lavsystems.rentacarmanagement.model.User
import com.lavsystems.rentacarmanagement.ui.login.LoginActivity
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils
import com.lavsystems.rentacarmanagement.ui.utils.ComponentUtils.showToast
import com.lavsystems.rentacarmanagement.ui.utils.DataState

class UserDialogFragment : DialogFragment() {
    private lateinit var binding: FragmentUserDialogBinding
    private lateinit var repository: UserRepository
    private lateinit var viewModel: UserViewModel
    private lateinit var userTypeAdapter: UserTypeAdapter
    private var user = User()
    private var userId = ""
    private val isCreate: Boolean
        get() = userId.isEmpty()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.Theme_App_Dialog_FullScreen)
        repository = UserRepositoryImpl(
            UserDataSourceImpl(
                SharedPreferencesImpl(requireContext()), UserService(
                    ConnectivityInterceptorImpl(requireContext()),
                    com.lavsystems.rentacarmanagement.BuildConfig.SERVER_URL
                )
            )
        )
        setUpViewModel()
        getBundleData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)
        return FragmentUserDialogBinding.inflate(inflater, container, false).also {
            binding = it
        }.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().window?.setWindowAnimations(
            R.style.DialogAnimation
        )

        setUpUI()
        setUpListener()
        setUpSpinnerAdapter()
        setUpObserverViewModel()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getFormUser(userId)
    }

    private fun setUpViewModel() {
        viewModel = ViewModelProvider(
            requireActivity(),
            UserViewModelFactory(repository)
        )[UserViewModel::class.java]
    }

    private fun setUpObserverViewModel() {
        with(viewModel) {
            userDTO.observe(this@UserDialogFragment) { state ->
                handleUiUser(state)
            }
            saveSuccess.observe(this@UserDialogFragment) { success ->
                success.getContentIfNotHandled()?.let {
                    if (it.success) {
                        cleanComponents()
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.user_added_success)
                        )
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.user_added_error, it.message)
                        )
                    }
                }
            }

            updateSuccess.observe(this@UserDialogFragment) { success ->
                success.getContentIfNotHandled()?.let {
                    if (it.success) {
                        cleanComponents()
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.user_updated_success)
                        )
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.user_updated_error, it.message)
                        )
                    }
                }
            }

            sessionExpired.observe(requireActivity()) { event ->
                event.getContentIfNotHandled()?.let {
                    ComponentUtils.showToast(requireContext(), getString(R.string.expired_session))
                    goToLogin()
                }
            }
        }
    }

    private fun cleanComponents() {
        with(binding) {
            edUserName.setText(EMPTY_STRING)
            edPass.setText(EMPTY_STRING)
            btnActions.text = requireActivity().getString(R.string.create_title)
        }
    }

    private fun setUpListener() {
        with(binding) {
            btnClosed.setOnClickListener {
                dismiss()
            }
            btnActions.setOnClickListener {
                if(setUserNameToUser() && setPassToUser()) {
                    if (!user.isRequiredEmptyData) {
                        if (isCreate)
                            viewModel.saveUser(user)
                        else
                            viewModel.updateUser(user)
                    } else {
                        showToast(
                            requireActivity(),
                            requireActivity().getString(R.string.required_datas_error)
                        )
                    }
                }
            }
        }
    }

    private fun setUserNameToUser(): Boolean {
        binding.edUserName.let { ed ->
            ed.text.toString().let {
                it.ifEmpty {
                    ed.error = requireActivity().getString(R.string.required_data_error)
                    return false
                }
                ed.error = null
                user.userName = it
                return true
            }
        }
    }

    private fun setPassToUser(): Boolean {
        binding.edPass.let { ed ->
            ed.text.toString().let {
                it.ifEmpty {
                    ed.error = requireActivity().getString(R.string.required_data_error)
                    return false
                }
                if(it.length < 4) {
                    ed.error = getString(R.string.four_length_error)
                    return false
                }
                ed.error = null
                user.password = it
                return true
            }
        }
    }

    private fun getBundleData() {
        arguments?.let {
            userId = it.getString(USER_ID_KEY, EMPTY_STRING)
        }
    }

    private fun setUpUI() {
        with(binding) {
            spType.apply {
                onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>?,
                        view: View?,
                        position: Int,
                        id: Long
                    ) {
                        userTypeAdapter.getUserTypeForPosition(position).let {
                            user.type = it
                        }
                    }

                    override fun onNothingSelected(parent: AdapterView<*>?) {
                    }
                }
            }
        }
    }

    private fun setUpSpinnerAdapter() {
        userTypeAdapter =
            UserTypeAdapter(requireActivity(), R.layout.simple_spinner_standar_item).also {
                binding.spType.adapter = it
            }
    }

    private fun handleUiUser(uiState: DataState<UserDTO>) {
        when (uiState) {
            is DataState.Success<UserDTO> -> {
                if (uiState.data.user == null && !isCreate) {
                    handlerErrorVisibility(true, EMPTY_STRING)
                    handlerProgressBarVisibility(false)
                    handlerContainerVisibility(false)
                    return
                }

                userTypeAdapter.setUserTypes(uiState.data.types)
                if (!isCreate) {
                    user = uiState.data.user!!
                    setUserUI()
                }

                handlerErrorVisibility(false, EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(true)
            }
            is DataState.Error -> {
                handlerErrorVisibility(true, uiState.throwable.message ?: EMPTY_STRING)
                handlerProgressBarVisibility(false)
                handlerContainerVisibility(false)
            }
            is DataState.Loading -> {
                handlerErrorVisibility(false, "")
                handlerProgressBarVisibility(true)
              //  handlerContainerVisibility(false)
            }
            is DataState.Idle -> Unit
        }
    }

    private fun setUserUI() {
        with(binding) {
            btnActions.text = requireActivity().getText(R.string.update_title)
            spType.setSelection(userTypeAdapter.getPositionByUserType(user.type))
            edUserName.setText(user.userName)
            edPass.setText(user.password)
        }
    }

    private fun handlerProgressBarVisibility(show: Boolean) {
        with(binding) {
            iProgressBar.progressBar.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerContainerVisibility(show: Boolean) {
        with(binding) {
            clContainer.visibility = if (show) View.VISIBLE else View.GONE
        }
    }

    private fun handlerErrorVisibility(show: Boolean, error: String) {
        with(binding) {
            iGenericError.clGenericError.visibility = if (show) View.VISIBLE else View.GONE
            iGenericError.tvMessage.text = error
            iGenericError.btnClose.setOnClickListener {
                iGenericError.clGenericError.visibility = View.GONE
                dismiss()
            }
        }

    }

    private fun goToLogin(){
        startActivity(Intent(requireContext(), LoginActivity::class.java))
        requireActivity().finish()
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
    }

    companion object {
        fun newInstance(id: String): UserDialogFragment {
            Bundle().apply {
                putString(USER_ID_KEY, id)
            }.also { b ->
                return UserDialogFragment().apply {
                    arguments = b
                }
            }
        }

        const val USER_ID_KEY = "user_id_key"
        const val USER_DIALOG_FRAGMENT_FLAG = "user_dialog_fragment_flag"
        const val EMPTY_STRING = ""
    }
}