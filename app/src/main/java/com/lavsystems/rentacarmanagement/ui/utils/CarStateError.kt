package com.lavsystems.rentacarmanagement.ui.utils

import com.lavsystems.rentacarmanagement.R
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
object CarStateError: CarState {
    override fun getState(): String {
        return CarState.ERROR
    }

    override fun getColor(): Int {
        return R.color.red_error
    }
}