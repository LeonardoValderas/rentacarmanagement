package com.lavsystems.rentacarmanagement.model

import com.lavsystems.rentacarmanagement.ui.utils.*
import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class BookingDetails(
    //@Json(name = "_id")
    @Transient
    val id: String = UUID.randomUUID().toString(),
    val car: Car,
    val date: Long,
    val bookings: MutableList<Booking>
) : ExpandableRecyclerViewAdapter.ExpandableGroup<Any>() {
    constructor() : this(
        "",
        Car(),
        0L,
        mutableListOf()
    )

    val nextBookingString: String
        get() {
            if(bookings.size > 1) {
              return NEXT_BOOKING.plus(bookings[1].startDateTime)
            }

            return NEXT_BOOKING.plus("-" )
        }

    override fun getExpandingItems(): MutableList<Any> {
        return bookings as MutableList<Any>
    }

    val carState: Pair<String, Int>
        get() {
            if(bookings.size > 0){
                val booking = bookings[0]
                val shotDate = date.toString().dropLast(8)
                return when {
                    booking.startDate == null -> {
                        Pair(CarStateError.getState(), CarStateError.getColor())
                    }
                    booking.startDate.toString().startsWith(shotDate) -> {
                        Pair(CarStateReservedToday.getState(), CarStateReservedToday.getColor())
                    }
                    booking.startDate!! > date -> {
                        Pair(CarStateAvailable.getState(), CarStateAvailable.getColor())
                    }
                    else -> {
                        Pair(CarStateReserved.getState(), CarStateReserved.getColor())
                    }
                }
            }

            return Pair(CarStateAvailable.getState(), CarStateAvailable.getColor())
        }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as BookingDetails

        if (id != other.id) return false
        if (car != other.car) return false
        if (bookings != other.bookings) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + car.hashCode()
        result = 31 * result + bookings.hashCode()
        return result
    }

    companion object {
        const val NEXT_BOOKING = "Proxima reserva: "
    }
}