package com.lavsystems.rentacarmanagement

import android.app.Application
import com.google.android.gms.ads.MobileAds

class RentACarManagerApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        MobileAds.initialize(this) {
//            AppOpenAdsManager(
//                this@RentACarManagerApplication
//            )
        }
    }
}