package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.datasource.BookingDataSource
import com.lavsystems.rentacarmanagement.data.dto.BookingDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.utils.NetworkException
import com.lavsystems.rentacarmanagement.data.api.utils.TokenException
import com.lavsystems.rentacarmanagement.data.api.utils.UnknownException
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.Booking
import com.lavsystems.rentacarmanagement.model.BookingDetails
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class BookingRepositoryImpl(private val dataSource: BookingDataSource) : BookingRepository {
    override fun getFormBooking(id: String): Flow<Response<ApiResponse<BookingDTO>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.getFormBooking(id).let {
                when (it) {
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e: Exception) {
            emit(Response.Error(e))
        }
    }

    override fun getBookingsByDate(date: String, onlyAvailable: Boolean): Flow<Response<ApiResponse<MutableList<BookingDetails>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.getBookingsByDate(date, onlyAvailable).let {
                when (it) {
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e: Exception) {
            emit(Response.Error(e))
        }
    }

    override fun hasStartOrEndDateBooking(
        id: String,
        carId: String,
        startDate: String,
        endDate: String
    ): Flow<Response<ApiResponse<Boolean>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.hasStartOrEndDateBooking(id, carId, startDate, endDate).let {
                when (it) {
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e: Exception) {
            emit(Response.Error(e))
        }
    }

    override fun saveBooking(date: String, onlyAvailable: Boolean, booking: Booking): Flow<Response<ApiResponse<MutableList<BookingDetails>>>> =
        flow {
            try {
                emit(Response.Loading)
                dataSource.saveBooking(date, onlyAvailable, booking).let {
                    when (it) {
                        is NetworkResponse.Success -> {
                            emit(Response.Success(it.body))
                        }
                        is NetworkResponse.ApiError -> {
                            emit(Response.Error(Exception(it.body.error)))
                        }
                        is NetworkResponse.NetworkError -> {
                            emit(Response.Error(NetworkException()))
                        }
                        is NetworkResponse.UnknownError -> {
                            emit(Response.Error(UnknownException()))
                        }
                        is NetworkResponse.TokenError -> {
                            emit(Response.Error(TokenException()))
                        }
                    }
                }
            } catch (e: Exception) {
                emit(Response.Error(e))
            }
        }

    override fun updateBooking(date: String, onlyAvailable: Boolean, booking: Booking): Flow<Response<ApiResponse<MutableList<BookingDetails>>>> =
        flow {
            try {
                emit(Response.Loading)
                dataSource.updateBooking(date, onlyAvailable, booking).let {
                    when (it) {
                        is NetworkResponse.Success -> {
                            emit(Response.Success(it.body))
                        }
                        is NetworkResponse.ApiError -> {
                            emit(Response.Error(Exception(it.body.error)))
                        }
                        is NetworkResponse.NetworkError -> {
                            emit(Response.Error(NetworkException()))
                        }
                        is NetworkResponse.UnknownError -> {
                            emit(Response.Error(UnknownException()))
                        }
                        is NetworkResponse.TokenError -> {
                            emit(Response.Error(TokenException()))
                        }
                    }
                }
            } catch (e: Exception) {
                emit(Response.Error(e))
            }
        }

    override fun deleteBooking(date: String, onlyAvailable: Boolean, id: String): Flow<Response<ApiResponse<MutableList<BookingDetails>>>> =
        flow {
            try {
                emit(Response.Loading)
                dataSource.deleteBooking(date, onlyAvailable, id).let {
                    when (it) {
                        is NetworkResponse.Success -> {
                            emit(Response.Success(it.body))
                        }
                        is NetworkResponse.ApiError -> {
                            emit(Response.Error(Exception(it.body.error)))
                        }
                        is NetworkResponse.NetworkError -> {
                            emit(Response.Error(NetworkException()))
                        }
                        is NetworkResponse.UnknownError -> {
                            emit(Response.Error(UnknownException()))
                        }
                        is NetworkResponse.TokenError -> {
                            emit(Response.Error(TokenException()))
                        }
                    }
                }
            } catch (e: Exception) {
                emit(Response.Error(e))
            }
        }
}