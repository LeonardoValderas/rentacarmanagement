package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.datasource.CarDataSource
import com.lavsystems.rentacarmanagement.data.dto.CarDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.utils.NetworkException
import com.lavsystems.rentacarmanagement.data.api.utils.TokenException
import com.lavsystems.rentacarmanagement.data.api.utils.UnknownException
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.Car
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CarRepositoryImpl(private val dataSource: CarDataSource) : CarRepository {
    override fun getCars(): Flow<Response<ApiResponse<MutableList<Car>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.getCars().let {
                when (it) {
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e: Exception) {
            emit(Response.Error(e))
        }
    }

    override fun getFormCar(id: String): Flow<Response<ApiResponse<CarDTO>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.getFormCar(id).let {
                when (it) {
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e: Exception) {
            emit(Response.Error(e))
        }
    }

    override fun saveCar(car: Car): Flow<Response<ApiResponse<MutableList<Car>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.saveCar(car).let {
                when (it) {
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e: Exception) {
            emit(Response.Error(e))
        }
    }


    override fun updateCar(car: Car): Flow<Response<ApiResponse<MutableList<Car>>>>  = flow {
        try {
            emit(Response.Loading)
            dataSource.updateCar(car).let {
                when(it){
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e:Exception){
            emit(Response.Error(e))
        }
    }

    override fun deleteCar(id: String): Flow<Response<ApiResponse<MutableList<Car>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.deleteCar(id).let {
                when(it){
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e:Exception){
            emit(Response.Error(e))
        }
    }
}