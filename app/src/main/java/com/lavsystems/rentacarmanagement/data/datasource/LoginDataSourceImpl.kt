package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.request.LoginRequest
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.api.service.LoginService
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferences
import com.lavsystems.rentacarmanagement.model.User

class LoginDataSourceImpl(private val sharedPreferences: SharedPreferences, private val  service: LoginService) : LoginDataSource {
    override suspend fun doLogin(userName: String, password: String): NetworkResponse<ApiResponse<User>, ApiResponseError> {
        return service.doLogin(LoginRequest( userName, password))
    }

    override suspend fun saveToken(token: String): Boolean {
        return sharedPreferences.saveToken(token)
    }

    override suspend fun saveUser(user: User): Boolean {
        return sharedPreferences.saveUser(user)
    }
}