package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.dto.BookingDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.Booking
import com.lavsystems.rentacarmanagement.model.BookingDetails
import kotlinx.coroutines.flow.Flow

interface BookingDataSource {
   suspend fun getBookingsByDate(date:String, onlyAvailable: Boolean): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>
   suspend fun hasStartOrEndDateBooking(id: String, carId: String, startDate:String, endDate: String): NetworkResponse<ApiResponse<Boolean>, ApiResponseError>
   suspend fun getFormBooking(id: String): NetworkResponse<ApiResponse<BookingDTO>, ApiResponseError>
   suspend fun saveBooking(date:String, onlyAvailable: Boolean, booking: Booking): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>
   suspend fun updateBooking(date:String, onlyAvailable: Boolean, booking: Booking): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>
   suspend fun deleteBooking(date:String, onlyAvailable: Boolean, id: String): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>
}