package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.datasource.UserDataSource
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.utils.NetworkException
import com.lavsystems.rentacarmanagement.data.api.utils.TokenException
import com.lavsystems.rentacarmanagement.data.api.utils.UnknownException
import com.lavsystems.rentacarmanagement.data.dto.UserDTO
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class UserRepositoryImpl(private val dataSource: UserDataSource): UserRepository {
    override fun getUsers(): Flow<Response<ApiResponse<MutableList<User>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.getUsers().let {
                when(it){
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
       } catch (e:Exception){
           emit(Response.Error(e))
       }
    }

//    override fun getUser(id: String): Flow<Response<ApiResponse<User>>> = flow {
//        try {
//            val apiResponse = dataSource.getUser(id)
//            emit(Response.Success(apiResponse))
//        } catch (e:Exception){
//            emit(Response.Error(e))
//        }
//    }

    override fun getFormUser(id: String): Flow<Response<ApiResponse<UserDTO>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.getFormUser(id).let {
                when(it){
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e:Exception){
            emit(Response.Error(e))
        }
    }

    override fun saveUser(user: User): Flow<Response<ApiResponse<MutableList<User>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.saveUser(user).let {
                when(it){
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e:Exception){
            emit(Response.Error(e))
        }
    }

    override fun updateUser(user: User): Flow<Response<ApiResponse<MutableList<User>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.updateUser(user).let {
                when(it){
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e:Exception){
            emit(Response.Error(e))
        }
    }

    override fun deleteUser(id: String): Flow<Response<ApiResponse<MutableList<User>>>> = flow {
        try {
            emit(Response.Loading)
            dataSource.deleteUser(id).let {
                when(it){
                    is NetworkResponse.Success -> {
                        emit(Response.Success(it.body))
                    }
                    is NetworkResponse.ApiError -> {
                        emit(Response.Error(Exception(it.body.error)))
                    }
                    is NetworkResponse.NetworkError -> {
                        emit(Response.Error(NetworkException()))
                    }
                    is NetworkResponse.UnknownError -> {
                        emit(Response.Error(UnknownException()))
                    }
                    is NetworkResponse.TokenError -> {
                        emit(Response.Error(TokenException()))
                    }
                }
            }
        } catch (e:Exception){
            emit(Response.Error(e))
        }
    }
}