package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.dto.CarDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.Car
import kotlinx.coroutines.flow.Flow

interface CarRepository {
    fun getCars(): Flow<Response<ApiResponse<MutableList<Car>>>>
    fun getFormCar(id: String): Flow<Response<ApiResponse<CarDTO>>>
    fun saveCar(car: Car): Flow<Response<ApiResponse<MutableList<Car>>>>
    fun updateCar(car: Car): Flow<Response<ApiResponse<MutableList<Car>>>>
    fun deleteCar(id: String): Flow<Response<ApiResponse<MutableList<Car>>>>
}