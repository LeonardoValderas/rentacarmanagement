package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.dto.UserDTO
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow

interface UserRepository {
    fun getUsers(): Flow<Response<ApiResponse<MutableList<User>>>>
//    fun getUser(id: String): Flow<Response<ApiResponse<User>>>
    fun getFormUser(id: String): Flow<Response<ApiResponse<UserDTO>>>
    fun saveUser(user: User): Flow<Response<ApiResponse<MutableList<User>>>>
    fun updateUser(user: User): Flow<Response<ApiResponse<MutableList<User>>>>
    fun deleteUser(id: String): Flow<Response<ApiResponse<MutableList<User>>>>
}