package com.lavsystems.rentacarmanagement.data.api.service

import com.lavsystems.rentacarmanagement.data.api.Retrofit
import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptor
import com.lavsystems.rentacarmanagement.data.dto.BookingDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.model.Booking
import com.lavsystems.rentacarmanagement.model.BookingDetails
import retrofit2.http.*

interface BookingService {
    @GET("bookings/companies/{companyId}/date")
    suspend fun getBookingsByDate(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Query("date") date: String?,
        @Query("onlyAvailable") onlyAvailable: Boolean?
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>

    @GET("bookings/cars/{carId}/validation/endDate/{id}")
    suspend fun hasStartOrEndDateBooking(
        @Header("Authorization") token: String,
        @Path("id") id: String,
        @Path("carId") carId: String?,
        @Query("startDate") startDate: String?,
        @Query("endDate") endDate: String?
    ): NetworkResponse<ApiResponse<Boolean>, ApiResponseError>


    @GET("bookings/form/data/companies/{companyId}/{id}")
    suspend fun getFormBooking(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Path("id") id: String
    ): NetworkResponse<ApiResponse<BookingDTO>, ApiResponseError>


    @POST("bookings/companies/{companyId}")
    suspend fun saveBooking(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Query("date") date: String?,
        @Query("onlyAvailable") onlyAvailable: Boolean?,
        @Body booking: Booking
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>

    @PUT("bookings/companies/{companyId}/{id}")
    suspend fun updateBooking(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Path("id") id: String,
        @Query("date") date: String?,
        @Query("onlyAvailable") onlyAvailable: Boolean?,
        @Body booking: Booking
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>

    @DELETE("bookings/companies/{companyId}/{id}")
    suspend fun deleteBooking(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Path("id") id: String,
        @Query("date") date: String?,
        @Query("onlyAvailable") onlyAvailable: Boolean?
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: String
        ): BookingService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(BookingService::class.java)
        }
    }
}