package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.datasource.HomeDataSource
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow

class HomeRepositoryImpl(private val dataSource: HomeDataSource) : HomeRepository {
    override suspend fun getUser(): Flow<User?> {
      return dataSource.getUser()
    }
    override suspend fun logout() {
        dataSource.logout()
    }
}