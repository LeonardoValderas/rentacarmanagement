package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.service.HomeService
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferences
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow

class HomeDataSourceImpl(private val sharedPreferences: SharedPreferences, service: HomeService): HomeDataSource {
    override suspend fun getUser(): Flow<User?> = sharedPreferences.getUser()

    override suspend fun logout() {
        sharedPreferences.clear()
    }
}