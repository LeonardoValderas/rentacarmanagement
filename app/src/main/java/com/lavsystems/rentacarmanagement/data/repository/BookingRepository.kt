package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.dto.BookingDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.Booking
import com.lavsystems.rentacarmanagement.model.BookingDetails
import kotlinx.coroutines.flow.Flow

interface BookingRepository {
    fun getFormBooking(id: String): Flow<Response<ApiResponse<BookingDTO>>>
    fun getBookingsByDate(date:String, onlyAvailable: Boolean): Flow<Response<ApiResponse<MutableList<BookingDetails>>>>
    fun hasStartOrEndDateBooking(id: String, carId: String, startDate:String, endDate:String): Flow<Response<ApiResponse<Boolean>>>
    fun saveBooking(date:String, onlyAvailable: Boolean, booking: Booking): Flow<Response<ApiResponse<MutableList<BookingDetails>>>>
    fun updateBooking(date:String, onlyAvailable: Boolean, booking: Booking): Flow<Response<ApiResponse<MutableList<BookingDetails>>>>
    fun deleteBooking(date:String, onlyAvailable: Boolean, id: String): Flow<Response<ApiResponse<MutableList<BookingDetails>>>>
}