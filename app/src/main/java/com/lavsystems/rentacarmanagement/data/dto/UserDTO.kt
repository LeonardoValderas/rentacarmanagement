package com.lavsystems.rentacarmanagement.data.dto

import com.lavsystems.rentacarmanagement.model.*
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserDTO(
    val user: User?,
    val types: MutableList<UserType>
) {
    constructor() : this(
        User(),
        mutableListOf()
    )
}