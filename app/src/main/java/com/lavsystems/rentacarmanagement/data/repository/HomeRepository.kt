package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow

interface HomeRepository {
    suspend fun getUser(): Flow<User?>
    suspend fun logout()
}