package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.dto.CarDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.model.Car

interface CarDataSource {
   suspend fun getCars(): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>
   suspend fun getFormCar(id: String): NetworkResponse<ApiResponse<CarDTO>, ApiResponseError>
   suspend fun saveCar(car: Car): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>
   suspend fun updateCar(car: Car): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>
   suspend fun deleteCar(id: String): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>
}