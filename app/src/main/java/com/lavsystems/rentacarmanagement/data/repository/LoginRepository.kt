package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow

interface LoginRepository {
    fun doLogin(email: String, pass: String): Flow<Response<ApiResponse<User>>>
    fun saveUser(user: User): Flow<Boolean>
    fun saveToken(token: String): Flow<Boolean>
}