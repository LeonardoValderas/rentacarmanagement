package com.lavsystems.rentacarmanagement.data.api.utils

import java.io.IOException

class NoConnectivityException: IOException()