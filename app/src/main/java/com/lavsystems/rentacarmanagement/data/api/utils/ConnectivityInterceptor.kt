package com.lavsystems.rentacarmanagement.data.api.utils

import okhttp3.Interceptor

interface ConnectivityInterceptor: Interceptor