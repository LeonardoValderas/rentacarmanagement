package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.service.UserService
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.dto.UserDTO
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferences
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.first

class UserDataSourceImpl(private val sharedPreferences: SharedPreferences, private val service: UserService): UserDataSource {
    override suspend fun getUsers(): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError> {
        return service.getUsers(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first())
    }

    override suspend fun getFormUser(id: String): NetworkResponse<ApiResponse<UserDTO>, ApiResponseError> {
        return service.getFormUser(sharedPreferences.getToken().first(), id)
    }

    override suspend fun saveUser(user: User): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>  {
        return service.saveUser(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first(), user)
    }

    override suspend fun updateUser(user: User): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>  {
        return service.updateUser(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first(), user.id, user)
    }

    override suspend fun deleteUser(id: String): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError> {
        return service.deleteUser(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first(), id)
    }
}