package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.utils.NetworkException
import com.lavsystems.rentacarmanagement.data.api.utils.TokenException
import com.lavsystems.rentacarmanagement.data.api.utils.UnknownException
import com.lavsystems.rentacarmanagement.data.datasource.SplashDataSource
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class SplashRepositoryImpl(private val dataSource: SplashDataSource) : SplashRepository {
    override fun doAutoLogin(): Flow<Response<ApiResponse<User>>> =
        flow {
            try {
                emit(Response.Loading)
                dataSource.doAutoLogin().let {
                    when (it) {
                        is NetworkResponse.Success -> {
                            emit(Response.Success(it.body))
                        }
                        is NetworkResponse.ApiError -> {
                            emit(Response.Error(Exception(it.body.error)))
                        }
                        is NetworkResponse.NetworkError -> {
                            emit(Response.Error(NetworkException()))
                        }
                        is NetworkResponse.UnknownError -> {
                            emit(Response.Error(UnknownException()))
                        }
                        is NetworkResponse.TokenError -> {
                            emit(Response.Error(TokenException()))
                        }
                    }
                }
            } catch (e: Exception) {
                emit(Response.Error(e))
            }
        }

    override fun saveUser(user: User): Flow<Boolean> = flow {
        try {
            val saved = dataSource.saveUser(user)
            emit(saved)
        } catch (e: Exception) {
            e.printStackTrace()
            emit(false)
        }
    }
}