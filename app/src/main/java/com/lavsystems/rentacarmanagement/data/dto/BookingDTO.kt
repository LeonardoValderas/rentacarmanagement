package com.lavsystems.rentacarmanagement.data.dto

import com.lavsystems.rentacarmanagement.model.Booking
import com.lavsystems.rentacarmanagement.model.Car
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BookingDTO(
    val booking: Booking?,
    val cars: MutableList<Car>
) {
    constructor() : this(
        Booking(),
        mutableListOf()
    )
}