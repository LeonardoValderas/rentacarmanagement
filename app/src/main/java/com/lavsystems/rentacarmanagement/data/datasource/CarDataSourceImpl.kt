package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.service.CarService
import com.lavsystems.rentacarmanagement.data.dto.CarDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferences
import com.lavsystems.rentacarmanagement.model.Car
import kotlinx.coroutines.flow.first

class CarDataSourceImpl(private val sharedPreferences: SharedPreferences, private val service: CarService): CarDataSource {
    override suspend fun getCars(): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError> {
        return service.getCars(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first())
    }

    override suspend fun getFormCar(id: String): NetworkResponse<ApiResponse<CarDTO>, ApiResponseError> {
        return service.getFormCar(sharedPreferences.getToken().first(), id)
    }

    override suspend fun saveCar(car: Car): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError> {
        return service.saveCar(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first(), car)
    }

    override suspend fun updateCar(car: Car): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError> {
        return service.updateCar(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first(), car.id, car)
    }

    override suspend fun deleteCar(id: String): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError> {
        return service.deleteCar(sharedPreferences.getToken().first(), sharedPreferences.getCompanyId().first(), id)
    }
}