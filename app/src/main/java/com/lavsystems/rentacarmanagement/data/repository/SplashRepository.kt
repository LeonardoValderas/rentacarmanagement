package com.lavsystems.rentacarmanagement.data.repository

import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.utils.Response
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow

interface SplashRepository {
    fun doAutoLogin(): Flow<Response<ApiResponse<User>>>
    fun saveUser(user: User): Flow<Boolean>
}