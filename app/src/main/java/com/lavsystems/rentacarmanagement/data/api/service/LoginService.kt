package com.lavsystems.rentacarmanagement.data.api.service

import com.lavsystems.rentacarmanagement.data.api.Retrofit
import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.request.LoginRequest
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptor
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.model.User
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface LoginService {
    @POST("auth/login")
    suspend fun doLogin(@Body loginRequest: LoginRequest
    ): NetworkResponse<ApiResponse<User>, ApiResponseError>

    @GET("auth/token/{token}")
    suspend fun doAutoLogin(@Path("token") token: String?
    ): NetworkResponse<ApiResponse<User>, ApiResponseError>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: String
        ): LoginService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(LoginService::class.java)
        }
    }
}