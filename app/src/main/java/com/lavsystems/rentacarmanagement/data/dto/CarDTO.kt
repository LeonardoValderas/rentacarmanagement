package com.lavsystems.rentacarmanagement.data.dto

import com.lavsystems.rentacarmanagement.model.Brand
import com.lavsystems.rentacarmanagement.model.Car
import com.lavsystems.rentacarmanagement.model.Color

data class CarDTO(
    val car: Car?,
    val brands: MutableList<Brand>,
    val colors: MutableList<Color>
) {
    constructor() : this(
        Car(),
        mutableListOf(),
        mutableListOf()
    )
}