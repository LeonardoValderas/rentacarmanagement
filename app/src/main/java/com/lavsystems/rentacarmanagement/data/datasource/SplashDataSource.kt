package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.model.User

interface SplashDataSource {
   suspend fun doAutoLogin(): NetworkResponse<ApiResponse<User>, ApiResponseError>
   suspend fun saveUser(user: User): Boolean
}