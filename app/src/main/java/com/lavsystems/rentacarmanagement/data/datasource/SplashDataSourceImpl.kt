package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.api.service.LoginService
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferences
import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.first

class SplashDataSourceImpl(private val sharedPreferences: SharedPreferences, private val  service: LoginService) : SplashDataSource {
    override suspend fun doAutoLogin(): NetworkResponse<ApiResponse<User>, ApiResponseError> {
      return service.doAutoLogin(sharedPreferences.getToken().first())
    }

    override suspend fun saveUser(user: User): Boolean {
        return sharedPreferences.saveUser(user)
    }
}