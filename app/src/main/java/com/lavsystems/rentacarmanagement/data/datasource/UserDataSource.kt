package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.dto.UserDTO
import com.lavsystems.rentacarmanagement.model.User

interface UserDataSource {
   suspend fun getUsers(): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>
//   suspend fun getUser(id: String): NetworkResponse<ApiResponse<User>, ApiResponseError>
   suspend fun getFormUser(id: String): NetworkResponse<ApiResponse<UserDTO>, ApiResponseError>
   suspend fun saveUser(car: User): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>
   suspend fun updateUser(car: User): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>
   suspend fun deleteUser(id: String): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>
}