package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.model.User
import kotlinx.coroutines.flow.Flow

interface HomeDataSource {
   suspend fun getUser(): Flow<User?>
   suspend fun logout()
}