package com.lavsystems.rentacarmanagement.data.api.service

import com.lavsystems.rentacarmanagement.data.api.Retrofit
import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptor
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.dto.UserDTO
import com.lavsystems.rentacarmanagement.model.User
import retrofit2.http.*

interface UserService {
    @POST("users/companies/{companyId}")
    suspend fun saveUser(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Body user: User
    ): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>

    @PUT("users/companies/{companyId}/{id}")
    suspend fun updateUser(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Path("id") id: String,
        @Body user: User
    ): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>

    @DELETE("users/companies/{companyId}/{id}")
    suspend fun deleteUser(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Path("id") id: String
    ): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>

    @GET("users/companies/{companyId}")
    suspend fun getUsers(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?
    ): NetworkResponse<ApiResponse<MutableList<User>>, ApiResponseError>

    @GET("users/form/data/{id}")
    suspend fun getFormUser(
        @Header("Authorization") token: String,
        @Path("id") id: String
    ): NetworkResponse<ApiResponse<UserDTO>, ApiResponseError>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: String
        ): UserService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(UserService::class.java)
        }
    }
}