package com.lavsystems.rentacarmanagement.data.api.service

import com.lavsystems.rentacarmanagement.data.api.Retrofit
import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.utils.ConnectivityInterceptor
import com.lavsystems.rentacarmanagement.data.dto.CarDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.model.Car
import retrofit2.http.*

interface CarService {
    @POST("cars/companies/{companyId}")
    suspend fun saveCar(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Body car: Car
    ): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>

    @PUT("cars/companies/{companyId}/{id}")
    suspend fun updateCar(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Path("id") id: String,
        @Body car: Car
    ): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>

    @GET("cars/companies/{companyId}")
    suspend fun getCars(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?
    ): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>

//    @GET("cars/{carId}/users/{userId}/companies/{companyId}")
//    suspend fun getCar(
//        @Header("Authorization") token: String,
//        @Path("carId") carId: String?,
//        @Path("userId") userId: String?,
//        @Path("companyId") companyId: String?
//    ): ApiResponse<CarDTO>

    @GET("cars/form/data/{id}")
    suspend fun getFormCar(
        @Header("Authorization") token: String,
        @Path("id") id: String
    ): NetworkResponse<ApiResponse<CarDTO>, ApiResponseError>

    @DELETE("cars/companies/{companyId}/{id}")
    suspend fun deleteCar(
        @Header("Authorization") token: String,
        @Path("companyId") companyId: String?,
        @Path("id") id: String
    ): NetworkResponse<ApiResponse<MutableList<Car>>, ApiResponseError>

    companion object {
        operator fun invoke(
            connectivityInterceptor: ConnectivityInterceptor,
            baseUrl: String
        ): CarService {
            return Retrofit
                .invoke(connectivityInterceptor, baseUrl)
                .create(CarService::class.java)
        }
    }
}