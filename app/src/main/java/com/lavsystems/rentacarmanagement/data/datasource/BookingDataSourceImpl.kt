package com.lavsystems.rentacarmanagement.data.datasource

import com.lavsystems.rentacarmanagement.data.api.adapter.NetworkResponse
import com.lavsystems.rentacarmanagement.data.api.service.BookingService
import com.lavsystems.rentacarmanagement.data.dto.BookingDTO
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponse
import com.lavsystems.rentacarmanagement.data.api.response.ApiResponseError
import com.lavsystems.rentacarmanagement.data.utils.SharedPreferences
import com.lavsystems.rentacarmanagement.model.BookingDetails
import com.lavsystems.rentacarmanagement.model.Booking
import kotlinx.coroutines.flow.first

class BookingDataSourceImpl(
    private val sharedPreferences: SharedPreferences,
    private val service: BookingService
) : BookingDataSource {
    override suspend fun getBookingsByDate(
        date: String,
        onlyAvailable: Boolean
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError> {
        return service.getBookingsByDate(
            sharedPreferences.getToken().first(),
            sharedPreferences.getCompanyId().first(),
            date,
            onlyAvailable
        )
    }

    override suspend fun hasStartOrEndDateBooking(
        id: String,
        carId: String,
        startDate: String,
        endDate: String
    ): NetworkResponse<ApiResponse<Boolean>, ApiResponseError> {
        return service.hasStartOrEndDateBooking(
            sharedPreferences.getToken().first(),
            id,
            carId,
            startDate,
            endDate
        )
    }

    override suspend fun getFormBooking(id: String): NetworkResponse<ApiResponse<BookingDTO>, ApiResponseError> {
        return service.getFormBooking(
            sharedPreferences.getToken().first(),
            sharedPreferences.getCompanyId().first(),
            id
        )
    }

    override suspend fun saveBooking(
        date: String,
        onlyAvailable: Boolean,
        booking: Booking
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError> {
        return service.saveBooking(
            sharedPreferences.getToken().first(),
            sharedPreferences.getCompanyId().first(),
            date,
            onlyAvailable,
            booking
        )
    }

    override suspend fun updateBooking(
        date: String,
        onlyAvailable: Boolean,
        booking: Booking
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError> {
        return service.updateBooking(
            sharedPreferences.getToken().first(),
            sharedPreferences.getCompanyId().first(),
            booking.id,
            date,
            onlyAvailable,
            booking
        )
    }

    override suspend fun deleteBooking(
        date: String,
        onlyAvailable: Boolean,
        id: String
    ): NetworkResponse<ApiResponse<MutableList<BookingDetails>>, ApiResponseError> {
        return service.deleteBooking(
            sharedPreferences.getToken().first(),
            sharedPreferences.getCompanyId().first(),
            id,
            date,
            onlyAvailable
        )
    }
}